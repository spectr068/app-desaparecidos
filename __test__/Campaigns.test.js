import React from 'react';
import Campaigns from '../screens/Campaigns';

import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<Campaigns />).toJSON();
  expect(tree).toMatchSnapshot();
});
