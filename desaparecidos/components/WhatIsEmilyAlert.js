import React, { Fragment } from 'react';
import { StyleSheet, Text } from 'react-native';

export default class WhatIsEmilyAlert extends React.Component {
  render() {
    return (
      <Fragment>
        <Text style={styles.presentationText}>
          Alerta EMILIA es un sistema de notificación de menores de edad desaparecidos considerados
          en ALTO RIESGO. En el cual se organizan las instituciones, sociedad civil y los medios de
          comunicación para su pronta y oportuna localización.
        </Text>
        <Text style={styles.presentationText}>
          Los mensajes de alerta son especialmente útiles, dado que la celeridad a la hora de actuar
          es vital.
        </Text>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  presentationText: {
    color: '#053862',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify'
  }
});
