import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  Dimensions,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default class MenuDrawer extends Component {
  navLink(nav, text) {
    return (
      <TouchableOpacity style={{ height: 50 }} onPress={() => this.props.navigation.navigate(nav)}>
        <Text style={styles.link}>{text}</Text>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        {/*<View style={styles.topLinks}>*/}
          {/*<Image*/}
            {/*style={styles.footerView_logo}*/}
            {/*source={require('../assets/logoMinisterioGobierno.png')}*/}
            {/*resizeMode="stretch"*/}
          {/*/>*/}
        {/*</View>*/}
        {/*<View style={styles.bottomLinks}>*/}
          {/*{this.navLink('Home', 'Inicio')}*/}
          {/*{this.navLink('Info', 'Información')}*/}
          {/*{this.navLink('Places', 'Lugares donde denunciar')}*/}
          {/*{this.navLink('MissingPeople', 'Personas desaparecidas')}*/}
          {/*{this.navLink('FindingFamily', 'Busco a mis familiares')}*/}
          {/*{this.navLink('SolvedCases', 'Casos resueltos')}*/}
          {/*{this.navLink('Campaigns', 'Campañas')}*/}
          {/*{this.navLink('EmilyAlert', 'Alerta Emilia')}*/}
          {/*{this.navLink('UserRegistration', 'Registro de usuarios')}*/}
          {/*{this.navLink('UnknownPeopleRegistration', 'Registro de personas NN ')}*/}
           {/*/!*{this.navLink('RegisterForm', 'RegisterForm')}*!/*/}
           {/*/!*{this.navLink('TEST', 'Test Camara')}*!/*/}
        {/*</View>*/}
        {/*<View style={styles.bottomBanner}>*/}
          {/*<Image*/}
            {/*style={styles.footerView_logo}*/}
            {/*source={require('../assets/footer.png')}*/}
            {/*resizeMode="stretch"*/}
          {/*/>*/}
        {/*</View>*/}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray'
  },
  topLinks: {
    height: 160
  },
  bottomLinks: {
    flex: 8,
    backgroundColor: '#fff',
    paddingTop: 10
  },
  link: {
    flex: 1,
    fontSize: 16,
    padding: 6,
    paddingLeft: 14,
    margin: 5,
    textAlign: 'left'
  },
  bottomBanner: {
    flex: 1
  },
  footerView_logo: {
    width: '100%',
    height: '100%'
  }
});
