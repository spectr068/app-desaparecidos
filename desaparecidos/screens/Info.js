import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import MenuButton from '../components/MenuButton';
// import Accordion from 'react-native-collapsible/Accordion';
import { FontAwesome } from '@expo/vector-icons';
import Constants from 'expo-constants';

import Presentation from '../components/Presentation';
import MissingRelativesInstructions from '../components/MissingRelativesInstructions';
import WhatToDo from '../components/WhatToDo';
import WhatNotToDo from '../components/WhatNotToDo';
import MoreInformation from '../components/MoreInformation';
import { ScrollView } from 'react-native-gesture-handler';

export default class Info extends React.Component {
  state = {
    activeSections: []
  };

  _renderHeader = (section, index, isActive, sections) => {
    return (
      <View style={styles.accordionItem_header}>
        <Text style={styles.accordionItem_headerText}>{section.title}</Text>
        <FontAwesome name={isActive ? 'minus' : 'plus'} size={20} color="white" />
      </View>
    );
  };

  _renderContent = section => {
    if (section.key === 'presentation') {
      return (
        <View style={styles.accordionItem_content}>
          <Presentation />
        </View>
      );
    }
    if (section.key === 'missing-relative-instructions') {
      return (
        <View style={styles.accordionItem_content}>
          <MissingRelativesInstructions />
        </View>
      );
    }
    if (section.key === 'what-to-do') {
      return (
        <View style={styles.accordionItem_content}>
          <WhatToDo />
        </View>
      );
    }
    if (section.key === 'what-not-to-do') {
      return (
        <View style={styles.accordionItem_content}>
          <WhatNotToDo />
        </View>
      );
    }
    if (section.key === 'more-information') {
      return (
        <View style={styles.accordionItem_content} scrollEnabled={true}>
          <MoreInformation />
        </View>
      );
    }
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  render() {
    return (
      <View style={styles.container}>
        <MenuButton navigation={this.props.navigation} />
        <View style={styles.headerView}>
          <Image style={styles.headerView_logo} source={require('../assets/main-logo.png')} />
        </View>
        <ScrollView style={styles.contentContainer}>
          {/*<Accordion*/}
          {/*  sections={SECTIONS}*/}
          {/*  activeSections={this.state.activeSections}*/}
          {/*  underlayColor="#03C8D5"*/}
          {/*  renderHeader={this._renderHeader}*/}
          {/*  renderContent={this._renderContent}*/}
          {/*  onChange={this._updateSections}*/}
          {/*  containerStyle={styles.accordionContainer}*/}
          {/*/>*/}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerView: {
    width: '50%',
    height: 80,
    alignSelf: 'center',
    marginTop: Constants.statusBarHeight
  },
  headerView_logo: {
    width: '100%',
    height: '100%'
  },
  contentContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#0E406B'
  },
  accordionContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: '#0E406B'
  },
  accordionItem_header: {
    backgroundColor: '#0E406B',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    height: 70
  },
  accordionItem_headerText: {
    fontSize: 20,
    color: '#fff'
  },
  accordionItem_content: {
    paddingVertical: 10,
    backgroundColor: '#03C8D5'
  }
});

const SECTIONS = [
  {
    title: 'Presentación',
    key: 'presentation'
  },
  {
    title: '¿Qué hacer en caso de desaparición?',
    key: 'missing-relative-instructions'
  },
  {
    title: '¿Qué necesito hacer?',
    key: 'what-to-do'
  },
  {
    title: '¿Qué no debo hacer?',
    key: 'what-not-to-do'
  },
  {
    title: 'Más información',
    key: 'more-information'
  }
];
