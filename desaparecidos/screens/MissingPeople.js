import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Image,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  Modal,
  ScrollView,
  TextInput
} from 'react-native';
import MenuButton from '../components/MenuButton';
import Constants from 'expo-constants';
// import axios from 'axios';

import MissingPeopleCard from '../components/MissingPeopleCard';
import {NavigationContainer} from "@react-navigation/native";
import MissingPeopleButton from "../components/MissingPeopleButton";
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import {Button} from "react-native-elements";
import ActionSheet from "react-native-actionsheet";



export default class MissingPeople extends React.Component {
  state = {
    missingPeople: [],
    next: '',
    modalVisible: false,


    idColorcabello: '',
    colorCabello: [],


    sexo: [],
    colorOjos: [],

    idSexo: '',
    idColorojos: '',

    colorCabelloT: [],
    colorCabelloI: [],
    colorCabelloA: null,


    colorOjosT: [],
    colorOjosI: [],
    colorOjosA: null,


    sexoT: [],
    sexoI: [],
    sexoA: null,


    busquedaNombre: '',

  };
  /////////////////////////////////////////////////////////////////////////////////////////////////////


  getCatalogCabello = async () => {
    const url =
        'http://desaparecidosecuador.gob.ec/catalogo/colorCabello';

    fetch(url, {headers: { 'Usuario': 'consumo_mdg_2020', 'Clave': 'Mdg2018UIO2020--1!','content-type': 'application/json' }}).then(response => response.json())
        .then(datos => {
          console.log('data parameters');
          // this.setState({ colorCabello: datos });

          this.setState({ colorCabello: datos });
          let i;
          for (i=0; i<datos.length; i++){
            this.state.colorCabelloT.push(datos[i].descripcion);
            this.state.colorCabelloI.push(datos[i].idcolorcabello);
          }

        }).catch(error => {
      console.log('error',error);

    });

    // let datos = JSON.parse(JSON.stringify(data));
    // this.setState({ colorCabello: datos });
    // let i;
    // for (i=0; i<datos.length; i++){
    //   this.state.colorCabelloT.push(datos[i].descripcion);
    //   this.state.colorCabelloI.push(datos[i].idcolorcabello);
    // }

  };

  getCatalogOjos = async () => {
    const url =
        'http://desaparecidosecuador.gob.ec/catalogo/colorOjos';

    fetch(url, {headers: { 'Usuario': 'consumo_mdg_2020', 'Clave': 'Mdg2018UIO2020--1!','content-type': 'application/json' }}).then(response => response.json())
        .then(datos => {
          console.log('data');
          // this.setState({ colorCabello: datos });

          this.setState({ colorOjos: datos });
          let i;
          for (i=0; i<datos.length; i++){
            this.state.colorOjosT.push(datos[i].descripcion);
            this.state.colorOjosI.push(datos[i].idcolorojos);
          }
          // console.log(this.state.colorOjosT);

        }).catch(error => {
      console.log('error',error);
    });



    // const { data } = await axios.get(url, {
    //   headers: { 'Usuario': 'consumo_mdg_2020', 'Clave': 'Mdg2018UIO2020--1!','content-type': 'application/json' }
    // });
    // let datos = JSON.parse(JSON.stringify(data));
    // this.setState({ colorOjos: datos });
    // let i;
    // for (i=0; i<datos.length; i++){
    //   this.state.colorOjosT.push(datos[i].descripcion);
    //   this.state.colorOjosI.push(datos[i].idcolorojos);
    // }
    // console.log(this.state.colorOjosT);


  };

  getCatalogSexo = async () => {
    const url =
        'http://desaparecidosecuador.gob.ec/catalogo/sexo';
    // const { data } = await axios.get(url, {
    //   headers: { 'Usuario': 'consumo_mdg_2020', 'Clave': 'Mdg2018UIO2020--1!','content-type': 'application/json' }
    // });
    fetch(url, {headers: { 'Usuario': 'consumo_mdg_2020', 'Clave': 'Mdg2018UIO2020--1!','content-type': 'application/json' }}).then(response => response.json())
        .then(datos => {
          console.log('data');
          // this.setState({ colorCabello: datos });

          // let datos = JSON.parse(JSON.stringify(data));
          this.setState({ sexo: datos });
          let i;
          for (i=0; i<datos.length; i++){
            this.state.sexoT.push(datos[i].descripcion);
            this.state.sexoI.push(datos[i].idsexo);
          }

        }).catch(error => {
      console.log('error',error);
    });

    // let datos = JSON.parse(JSON.stringify(data));
    // this.setState({ sexo: datos });
    // let i;
    // for (i=0; i<datos.length; i++){
    //   this.state.sexoT.push(datos[i].descripcion);
    //   this.state.sexoI.push(datos[i].idsexo);
    // }

  };

  buscarDesaparecidos = async () => {
    const urlBase =
        'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/busqueda/desaparecidos/filtro?';
    let pagina = 1;
    let menor = false;
    let sexo = 'T';
    let colorCabello='T';
    let colorOjos='T';

    if (this.state.colorCabelloT[this.state.colorCabelloA]!== undefined && this.state.colorCabelloT[this.state.colorCabelloA]!== 'NO DETERMINADO'){
      colorCabello = this.state.colorCabelloT[this.state.colorCabelloA] ;
    }
    if (this.state.colorOjosT[this.state.colorOjosA]!== undefined && this.state.colorOjosT[this.state.colorOjosA]!== undefined !== 'OTROS') {
      colorOjos = this.state.colorOjosT[this.state.colorOjosA];
    }

    if (this.state.colorCabelloT[this.state.colorCabelloA]!== undefined && this.state.colorCabelloT[this.state.colorCabelloA]!== 'NO DETERMINADO') {
      if (this.state.sexoT[this.state.sexoA] === 'HOMBRE') {
        sexo = 'M';
      }
      if (this.state.sexoT[this.state.sexoA] === 'MUJER') {
        sexo = 'F';
      }
    }
    // pagina=1&menor=false&sexo=T&colorcabello=T&colorojos=T

    const url = urlBase + 'pagina='+ pagina + '&menor='+ menor + '&sexo='+ sexo + '&colorcabello=' + colorCabello+ '&colorojos='+colorOjos;

      console.log (url);


    fetch(url).then(response => response.json())
        .then(data => {
      this.setState({ missingPeople: data.registros, next: data.siguiente });

      // let datos = JSON.parse(JSON.stringify(data));
        console.log('consulta realizada');
        console.log(data.siguiente);
          this.setState({modalVisible:false})

    }).catch(error => {
      console.log(error);
    });

    // const { data } = await axios.get(url);


  };

  buscarDesaparecidosNombre = async() => {


    const url = `http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/busqueda/desaparecidos/${this.state.busquedaNombre.toUpperCase()}`;

    fetch(url).then(response => response.json())
        .then(data => {
          this.setState({ missingPeople: data.registros, next: data.siguiente });

          // let datos = JSON.parse(JSON.stringify(data));
          console.log('consulta realizada');
          console.log(data.siguiente);
          this.setState({modalVisible:false})

        }).catch(error => {
      console.log(error);
    });

  }





  /////////////////////////////////////////////////////////////////////////////////////////////////////
  componentDidMount() {
    this.getMissingPeople(1);
    this.getCatalogCabello();
    this.getCatalogOjos();
    this.getCatalogSexo();

  }

  getMissingPeople = async page => {

    const url = `http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/busqueda/desaparecidos/?pagina=${page}`;

    fetch(url).then(response => response.json())
        .then(datos => {
          console.log('missing people');
            this.setState({ missingPeople: datos.registros,
              next: datos.siguiente });

        }).catch(error => {
      console.log('error',error);
    });



    // try {
    //   const response = await axios.get(
    //     `http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/busqueda/desaparecidos/?pagina=${page}`
    //   );
    //   // console.log(response.data);
    //   this.setState({ missingPeople: response.data.registros, next: response.data.siguiente });
    // } catch (error) {
    //   console.error(error);
    // }
  };

  showMore = async () => {


    console.log('da', this.state.next);

    fetch(this.state.next).then(response => response.json())
        .then(datos => {
          console.log('data adicional');
          this.setState({
            missingPeople:
            this.state.missingPeople.concat(datos.registros),
            next: datos.siguiente
          });

        }).catch(error => {
      console.log('error',error);
    });


    // try {
    //   const response = await axios.get(this.state.next);
    //   this.setState({
    //     missingPeople: this.state.missingPeople.concat(response.data.registros),
    //     next: response.data.siguiente
    //   });
    // } catch (error) {
    //   console.error(error);
    // }
  };


  showFinder = () => {
    // this._menu.show();
    {/*<Text onPress={this.showMenu}>menú</Text>*/}
    console.log('press');
    this.setState({modalVisible:true})

  };

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.headerView}>
              <Text style={styles.headerText}>DESAPARECIDOS</Text>
              {/*<MissingPeopleButton navigation={this.props.navigation} />*/}
            <View style={styles.menuIcon}>
              <Ionicons style={styles.findIcon}  name="md-search" size={32}   onPress={this.showFinder}/>
            </View>
          </View>
        <FlatList
          style={styles.peopleContainer}
          data={this.state.missingPeople}
            keyExtractor={item => {item.idDesaparecido;
            // console.log(item);
            }}
          numColumns="1"
          renderItem={({ item }) => (
            <MissingPeopleCard

              key={item.idDesaparecido}
              idDesaparecido={item.idDesaparecido}
              cedula={item.cedula}
              nombres={item.nombres}
              pictureId={item.foto}
              fechaDesaparicion={item.fechaDesaparicion}
            />
          )}
          onEndReached={() => this.showMore()}
        />
        <Modal
            style={styles.modalContent}
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({ modalVisible: !this.state.modalVisible });
            }}
        >
          <View style={styles.modalInfo}>

            <View style={styles.modalInfoCloseButtonContainer}>
              <TouchableOpacity
                  onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}
                  style={{}}
              >
                <FontAwesome name="close" size={40} color="#053862" />
              </TouchableOpacity>
            </View>

            <View style = {styles.modalInfoContainer}>


              <Text></Text>


              <Text style={styles.formHeaders}>Búsqueda por características</Text>

              {/*<Text style={styles.textInfoPersonFin}>*/}
              {/*  Búsqueda por características*/}
              {/*</Text>*/}

              <View 
              style={styles.buttonSize}
              >
                <Button
                raised
                 onPress={
                  ()=> {this.ActionSheetColotCabello.show()
                  }}
                        // type="outline"
                        // raised={true}
                        titleStyle={styles.buttonText}
                        buttonStyle={{
                          backgroundColor: "white",
                          flex: 1,
                        }}
                        // fontSize={5}
                        // titleStyle={{ fontSize: 1 }}
                        title='Color Cabello:' />
                <ActionSheet
                    ref={o => this.ActionSheetColotCabello = o}
                    title={'Color Cabello:'}
                    options={this.state.colorCabelloT}
                    onPress={(index) => { this.setState({colorCabelloA: index});
                    }}
                />
                <Text style={styles.buttonText}>  {this.state.colorCabelloT[this.state.colorCabelloA]}</Text>
              </View>

              <View style={styles.buttonSize}>
                <Button onPress={
                  ()=> {this.ActionSheetSexo.show()
                  }}
                        type="outline"
                        raised={true}
                        titleStyle={styles.buttonText}
                        buttonStyle={{
                          backgroundColor: "white",
                          flex: 1,
                        }}
                        title='Sexo:' />
                <ActionSheet
                    ref={o => this.ActionSheetSexo = o}
                    title={'Sexo:'}
                    options={this.state.sexoT}
                    onPress={(index) => { this.setState({sexoA: index});
                    }}
                />
                <Text style={styles.buttonText}>  {this.state.sexoT[this.state.sexoA]}</Text>
              </View>

              <View style={styles.buttonSize}>
                <Button onPress={
                  ()=> {this.ActionSheetColorOjos.show()
                  }}
                        type="outline"
                        raised={true}
                        titleStyle={styles.buttonText}
                        buttonStyle={{
                          backgroundColor: "white",
                          flex: 1,
                        }}
                        title='Color Ojos:' />
                <ActionSheet
                    ref={o => this.ActionSheetColorOjos = o}
                    title={'Color Ojos:'}
                    options={this.state.colorOjosT}
                    onPress={(index) => { this.setState({colorOjosA: index});
                    }}
                />
                <Text style={styles.buttonText}>  {this.state.colorOjosT[this.state.colorOjosA]}</Text>
              </View>

              <View style={styles.buttonContainer}>
                <Button
                    buttonType="outline"
                    onPress={this.buscarDesaparecidos}
                    title="Buscar por filtros"
                    buttonColor="#fff"
                    backgroundColor="#053862"
                />
              </View>

              <Text style={styles.formHeaders}>Búsqueda por nombre</Text>


              <TextInput
                  style={styles.formInputs}
                  placeholder="Búsqueda por nombre"
                  placeholderTextColor = "#074d57"
                  value={this.state.busquedaNombre}
                  onChange={value => this.setState({ busquedaNombre: value.nativeEvent.text })}
              />


              <View style={styles.buttonContainer}>
                <Button
                    buttonType="outline"
                    onPress={this.buscarDesaparecidosNombre}
                    title="Buscar por nombre"
                    buttonColor="#fff"
                    backgroundColor="#053862"
                />
              </View>

            </View>
          </View>



        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#00ACBF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerText :{
      color: '#fff',
      fontSize: 25
  },

  headerView: {
    backgroundColor: '#074d57',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 80,
    alignSelf: 'center',
    marginTop: Constants.statusBarHeight /2,
    marginBottom: Constants.statusBarHeight /2,
  },
  headerView_logo: {
    width: '100%',
    height: '100%'
  },
  peopleContainer: {
    flex: 1,
    width: '100%'
  },
  showMoreButton: {
    height: 60,
    width: Dimensions.get('window').width,
    backgroundColor: '#053862',
    alignItems: 'center',
    justifyContent: 'center'
  },


  ///// modal

  modalInfoContainer: {
    // flex: 2,
    width: '90%',
    height: '85%',
    // alignItems: 'center',
    alignSelf: 'center',

    // backgroundColor: '#00ACBF',
    backgroundColor: '#00dfF8',
    borderColor: '#fff',
    borderWidth: 2,
    // height: 250,
    // padding: 10
  },

  modalContent:{
    // width: (Dimensions.get('window').width - 20),
    height: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: '#fff',


  },
  modalInfo: {
    // backgroundColor: '#fff',

    // backgroundColor: 'rgba(255,255,255,1)',
    // flex: 1
  },
  modalInfoCloseButtonContainer: {
    // flex: 1,
    // height: 20,
    alignItems: 'flex-end'
  },

  buttonSize:{
    width: '50%',
    // height: '10%',
    // flexDirection: 'row',
    alignSelf: 'center',
    marginTop: '1%',
    // marginBottom: 5,
    fontSize: (Dimensions.get('window').width /20) , // approximate a square

  },
  buttonText:{
    fontSize: (Dimensions.get('window').width /26) , // approximate a square
    color: '#074d57',
    // height: '20%',
    // marginBottom: '2%',
    alignSelf: 'center',

  },



    formHeaders: {
    width: '100%',
    height: 40,
    // backgroundColor: '#ECECEC',
    backgroundColor: '#00ACBF',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 14,
    // color: '#053862',
    color: '#fff',
    borderColor: '#A5A5A5',
    borderRadius: 5,
    marginTop: '2%',
    marginBottom: '2%',

  },



  menuIcon: {
    zIndex: 9,
    position: 'absolute',
    marginTop: Constants.statusBarHeight,

    top: 0,
    right: 20,
    justifyContent: 'center'
  },

  findIcon: {
    color: '#fff',
    borderColor: '#A5A5A5',

    // zIndex: 9,
    // position: 'absolute',
    // marginTop: Constants.statusBarHeight,
    //
    // top: 0,
    // right: 20,
    // justifyContent: 'center'
  },

  buttonContainer: {
    marginLeft: '15%',
    marginRight: '15%',

  },
  formInputs: {
    borderColor: '#ECECEC',
    backgroundColor: '#ECECEC',
    color: '#0000ff',


    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 5,
    height: 30,
    marginHorizontal: '10%',
    marginVertical: '3%'
    // marginLeft: '15%',
    // marginRight: '15%',
  },


});
