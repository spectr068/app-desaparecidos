import React from 'react';
import UnknownPeopleRegistration from '../screens/UnknownPeopleRegistration';

import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<UnknownPeopleRegistration />).toJSON();
  expect(tree).toMatchSnapshot();
});
