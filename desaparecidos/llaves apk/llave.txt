keyMinisterio



expo build:android -t app-bundle --clear-credentials



Keystore credentials
  Keystore password: keyMinisterio
  Key alias:         upload
  Key password:      


Here�s how to generate and register a new upload key:

Follow the instructions in the Android Studio Help Center to generate a new key. It must be different from any previous keys. Alternatively, you can use the following command line to generate a new key:
keytool -genkeypair -alias upload -keyalg RSA -keysize 2048 -validity 9125 -keystore keystore.jks

This key must be a 2048 bit RSA key and have 25-year validity.
Export the certificate for that key to PEM format:
keytool -export -rfc -alias upload -file upload_certificate.pem -keystore keystore.jks 

Reply to this email and attach the upload_certificate.pem file.



https://expo.io/artifacts/4fad0a0d-01b1-4db1-b86c-1a93de614fd8