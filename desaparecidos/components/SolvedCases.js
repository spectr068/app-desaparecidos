import React, { Fragment } from 'react';
import { StyleSheet, Text } from 'react-native';

export default class SolvedCases extends React.Component {
  state = {
    solvedCases: false
  };
  render() {
    return (
      <Fragment>
        {this.state.solvedCases ? (
          ''
        ) : (
          <Text style={styles.presentationText}>Al momento no existen casos</Text>
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  presentationText: {
    color: '#053862',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify'
  }
});
