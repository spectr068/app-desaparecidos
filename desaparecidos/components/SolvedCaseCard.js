import React, { Component, Fragment } from 'react';

import { View, StyleSheet, ImageBackground, Text } from 'react-native';

export default class SolvedCaseCard extends Component {
  render() {
    const { idDesaparecido, nombres } = this.props;
    return (
      <Fragment>
        <ImageBackground
          key={idDesaparecido}
          style={styles.missingPeopleContainer}
          source={require('../assets/encontrado.png')}
        >
          <View style={{ backgroundColor: '#fff', height: 60 }}>
            <Text style={{ alignSelf: 'center', textAlign: 'center' }}>{nombres}</Text>
            <Text style={{ alignSelf: 'center', textAlign: 'center' }}>Persona localizada</Text>
          </View>
        </ImageBackground>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  missingPeopleContainer: {
    flex: 1,
    height: 250,
    alignItems: 'center',
    flexDirection: 'column',
    margin: 5,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5
  }
});
