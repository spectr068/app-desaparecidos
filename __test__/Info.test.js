import React from 'react';
import Info from '../screens/Info';

import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<Info />).toJSON();
  expect(tree).toMatchSnapshot();
});
