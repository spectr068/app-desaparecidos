import React, { Fragment } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class MissingRelativesInstructions extends React.Component {
  render() {
    return (
      <Fragment>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info1.png')} />
          <Text style={styles.presentationText}>
            Generar una lista de contactos de las personas más cercanas a la persona desaparecida.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info2.png')} />
          <Text style={styles.presentationText}>
            Tener presente y recordar las actividades planificadas por la persona desaparecida en el
            momento que se detectó su ausencia.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info3.png')} />
          <Text style={styles.presentationText}>
            Acudir a la Unidad de Policía Comunitaria (UPC) o Fiscalía más cercana a su domicilio
            para informar lo sucedido o comunicarse directamente con la Policía Nacional o al 911.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info4.png')} />
          <Text style={styles.presentationText}>
            Proveer de información acerca de los hábitos de la persona desaparecida a los servidores
            policiales para orientar la búsqueda inmediata en el sector de la desaparición de la
            persona.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info5.png')} />
          <Text style={styles.presentationText}>
            Colaborar con las diligencias como entrevistas, consultas, llamadas, entre otras que
            realice el investigador.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info6.png')} />
          <Text style={styles.presentationText}>
            Difundir únicamente el afiche de información generado por la autoridad competente.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info7.png')} />
          <Text style={styles.presentationText}>
            Cuando la persona desaparecida retorne al hogar o haya sido localizada, informe a las
            autoridades.
          </Text>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  instructionContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10
  },
  instructionImage: {
    maxHeight: 80,
    maxWidth: 80,
    marginRight: 10,
    flex: 2
  },
  presentationText: {
    color: '#fff',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify',
    flex: 8
  }
});
