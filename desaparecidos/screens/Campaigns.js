import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import MenuButton from '../components/MenuButton';
import Constants from 'expo-constants';
import Carousel from 'react-native-banner-carousel';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 250;

const images = [
  require('../assets/campannas/meme-01.png'),
  require('../assets/campannas/meme-02.png'),
  require('../assets/campannas/meme-03.png'),
  require('../assets/campannas/meme-04.png'),
  require('../assets/campannas/meme-05.png')
];

export default class Campaigns extends React.Component {
  renderPage(image, index) {
    return (
      <View key={index}>
        <Image
          style={{ width: BannerWidth, height: BannerHeight }}
          source={image}
          resizeMode="contain"
        />
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        <MenuButton navigation={this.props.navigation} />
        <View style={styles.headerView}>
          <Image style={styles.headerView_logo} source={require('../assets/main-logo.png')} />
        </View>
        <ScrollView style={styles.contentContainer}>
          <Carousel autoplay autoplayTimeout={5000} loop index={0} pageSize={BannerWidth}>
            {images.map((image, index) => this.renderPage(image, index))}
          </Carousel>
          <Image
            style={{ width: BannerWidth, height: BannerHeight }}
            source={require('../assets/campannas/Gif-Desaparecidos1.gif')}
            resizeMode="stretch"
          />
          <Image
            style={{ width: BannerWidth, height: BannerHeight }}
            source={require('../assets/campannas/Gif-Desaparecidos2.gif')}
            resizeMode="stretch"
          />
        </ScrollView>
        <View style={styles.footerView}>
          <Image style={styles.footerView_logo} source={require('../assets/footer.png')} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerView: {
    width: '50%',
    height: 80,
    alignSelf: 'center',
    marginTop: Constants.statusBarHeight
  },
  headerView_logo: {
    width: '100%',
    height: '100%'
  },
  contentContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#0E406B'
  },
  footerView: {
    margin: 10,
    height: 60,
    width: '100%',
    alignSelf: 'stretch'
  },
  footerView_logo: {
    width: '100%',
    height: '100%'
  },
  memeImage: {
    width: '100%',
    height: 500
  }
});
