import React, { Component, Fragment } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, View, Text, ImageEditor, ImageStore  } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import { FontAwesome } from '@expo/vector-icons';
import * as MediaLibrary from "expo-media-library";
// import fs from 'react-native-fs';
// import * as FileSystem from 'expo-file-system';

import * as ImageManipulator from 'expo-image-manipulator';


const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default class CameraNN extends Component {
  camera = null;

  state = {
    hasCameraPermission: null
  };

  async componentDidMount() {
    const camera = await Permissions.askAsync(Permissions.CAMERA);
    // const audio = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    // const hasCameraPermission = camera.status === 'granted' && audio.status === 'granted';
    const hasCameraPermission = camera.status === 'granted';

    this.setState({ hasCameraPermission });
  }

  async takePicture() {
      const options = { quality: 0, base64: true, fixOrientation: true,
          exif: true, skipProcessing: true, ratio: "16:9", width: 480};

      console.log('ratio', this.camera.getSupportedRatiosAsync());

      this.camera.takePictureAsync(options).then((data) => {

          let width = data.width;
          let height = data.height;
          console.log(data.width);
          console.log(data.height);
          // console.log(data.base64);

         ImageManipulator.manipulateAsync(
             data.uri,
             [{ resize: { width: 480 } }],
             { compress: 1, format: ImageManipulator.SaveFormat.JPEG,  base64: true}
          ).then((imageManipulated) => {
             console.log(imageManipulated.uri) ;
             console.log(imageManipulated.width) ;
             console.log(imageManipulated.height) ;

             this.props.loadPicture(imageManipulated);
             this.onPictureSaved(imageManipulated).then();
             // console.log('base64',imageManipulated.base64) ;
             // infoImage = await FileSystem.getInfoAsync(message.image.uri);
          });



          // ImageManipulator.manipulateAsync(uri, actions, saveOptions)




          // const cropData = {
          //     offset: { x: 0, y: 0 },
          //     size: { width, height },
          //
          //     displaySize: { width: 640, height: 480 },
          // };
          //
          // ImageEditor.cropImage(data.uri, cropData, async (resizedImage) => {
          //     // resizedImage == 'file:///data/.../img.jpg'
          //     console.log('resizedImage',resizedImage)
          //     console.log(resizedImage);
          //
          //     const base64 = await FileSystem.readAsStringAsync(resizedImage, { encoding: 'base64' });
          //     console.log(base64);
          //
          //     //
          //     // ImageStore.getBase64ForTag(resizedImage, (data) => {
          //     //     // data == base64 encoded image
          //     //         console.log(data);
          //     //
          //     // }, e => console.warn("getBase64ForTag: ", e))
          //     //
          //
          //     // fs.readFile(resizedImage, 'base64').then(res => {
          //     //
          //     // })
          //     // .catch(err => {
          //     //     console.log(err.message, err.code);
          //     // });
          //
          //     // let datos=  await FileSystem.readAsStringAsync(resizedImage, 'base64');
          //     //     console.log(datos);
          //
          //     //
          //     // ImageStore.getBase64ForTag(resizedImage, (data2) => {
          //     //     // data == base64 encoded image
          //     //     console.log('getBase64ForTag',data2)
          //     //     // this.props.loadPicture({uri: resizedImage, data: data2});
          //     //     this.props.loadPicture(data);
          //     //
          //     // }, e => console.warn("getBase64ForTag: ", e))
          //     // this.onPictureSaved(resizedImage);
          //
          // }, (error) => {
          //     console.error('Error resizing image: ', error.getMessage());
          // });

      });

    // const photoData = await this.camera.takePictureAsync({ base64: true });
    // this.props.loadPicture(photoData);
    // this.onPictureSaved(photoData);

  }

    onPictureSaved = async photo => {
        this.setState({photo: photo.uri});


        console.log(photo);

        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === "granted") {


            const asset = await MediaLibrary.createAssetAsync(photo.uri);
            console.log(asset);

            await MediaLibrary.createAlbumAsync("Ministerio/fotos", asset, false)


        }
    }
  render() {
    const { hasCameraPermission } = this.state;

    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>El accesso a la cámara ha sido denegado</Text>;
    }
    return (
      <Fragment>
        <Camera
          style={styles.preview}
          type={Camera.Constants.Type.back}
          ref={camera => (this.camera = camera)}
        />
        <TouchableOpacity
          style={{
            position: 'absolute',
            bottom: 40,
            left: '45%'
          }}
          onPress={() => this.takePicture()}
        >
          <FontAwesome name="camera" size={70} color="#fff" />
        </TouchableOpacity>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    zIndex: 0
  }
});
