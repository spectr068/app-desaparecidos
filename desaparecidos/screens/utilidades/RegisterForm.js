import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Picker,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    KeyboardAvoidingView,
    TextInput,
    Image,
    Modal
} from 'react-native';

import MenuButton from '../../components/MenuButton';
import FormButton from '../../components/FormButton';
import CameraNN from '../../components/CameraNN';

import Constants from 'expo-constants';
import axios from 'axios';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import { FontAwesome } from '@expo/vector-icons';
import * as MediaLibrary from "expo-media-library";

import ActionSheet from 'react-native-actionsheet'

import { Button } from 'react-native-elements';

import { Dialog } from 'react-native-simple-dialogs';
import { ProgressDialog  } from 'react-native-simple-dialogs';

export default class RegisterForm extends React.Component {
    state = {
        nombre: '',
        apodo: '',
        trasladoDe: '',
        lugarAcogida: '',
        referenciasFamiliares: '',
        edad: '',
        estatura: '',
        seniales: '',
        contextura: [],
        sexo: [],
        colorOjos: [],
        colorCabello: [],
        colorPiel: [],
        discapacidad: [],
        senial: [],
        token: '',
        idContextura: '',
        idSexo: '',
        idColorcabello: '',
        idColorpiel: '',
        idColorojos: '',
        idDiscapacidad: '',
        idSenial: '',
        mapRegion: {
            latitude: -0.1765486,
            longitude: -78.4831857
        },
        cameraVisible: false,
        foto: null,

        documentoIdentidad:'',
        condicionesEncontrado:'',
        dondeFueEncontrado:'',
        edadAparente:'',

        contexturaT: [],
        contexturaI: [],
        contexturaA: null,

        sexoT: [],
        sexoI: [],
        sexoA: null,

        condicionesEncontradoT:[],
        condicionesEncontradoA:null,
        condicionesEncontradoI:[],

        edadAparenteT:[],
        edadAparenteA:null,
        edadAparenteI:[],


        colorCabelloT: [],
        colorCabelloI: [],
        colorCabelloA: null,

        colorPielT: [],
        colorPielI: [],
        colorPielA: null,

        colorOjosT: [],
        colorOjosI: [],
        colorOjosA: null,

        discapacidadT: [],
        discapacidadI: [],
        discapacidadA: null,

        senialT: [],
        senialI: [],
        senialA: null,


        dialogVisible: false,
        dialogoRegistro: false
    };
    goToUserLogin() {}


    // constructor() {
    //     super();
    //
    //     this.state = {
    //         textInputValue: ''
    //     }
    // }

    getCurrentLocation = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            console.log('Permission to access location was denied');
        } else {
            console.log('Permission to access location granted');
        }
        let location = await Location.getCurrentPositionAsync({
            accuracy: Location.Accuracy.BestForNavigation
        });

        this.setState({
            mapRegion: {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude
            }
        });

        Location.watchPositionAsync(
            {
                accuracy: Location.Accuracy.BestForNavigation,
                timeInterval: 1000,
                distanceInterval: 10000
            },
            location =>
                this.setState({
                    mapRegion: {
                        latitude: location.coords.latitude,
                        longitude: location.coords.longitude
                    }
                })
        );
    };

    registerNN = async () => {

        // console.log(this.state.foto.slice(0,100));


        const {
            contexturaA,
            sexoA,
            colorCabelloA,
            colorPielA,
            colorOjosA,
            discapacidadA,
            senialA,
            edadAparenteA,
            condicionesEncontradoA
        } = this.state;

        console.log( contexturaA,
            sexoA,
            colorCabelloA,
            colorPielA,
            colorOjosA,
            discapacidadA,
            senialA);

        console.log(this.state.colorCabelloT)

        this.state.idContextura = this.state.contexturaI[contexturaA];
        this.state.idSexo = this.state.sexoI[sexoA];
        this.state.idColorcabello= this.state.colorCabelloI[colorCabelloA];
        this.state.idColorpiel = this.state.colorPielI[colorPielA];
        this.state.idColorojos = this.state.colorOjosI[colorOjosA];
        this.state.idDiscapacidad = this.state.discapacidadI[discapacidadA];
        this.state.idSenial = this.state.senialI[senialA];
        this.state.idEdadAparente = this.state.edadAparenteI[edadAparenteA];
        // this.state.idCondicionesEncontrado = this.state.condicionesEncontradoI[condicionesEncontradoA];
        let condicionEntrontrado = this.state.condicionesEncontradoT[condicionesEncontradoA];




        const {
            nombre,
            apodo,
            trasladoDe,
            lugarAcogida,
            referenciasFamiliares,
            edad,
            estatura,
            seniales,
            idContextura,
            idSexo,
            idColorcabello,
            idColorpiel,
            idColorojos,
            idDiscapacidad,
            idSenial,
            foto,

            documentoIdentidad,
            dondeFueEncontrado,
            idEdadAparente,
            // idCondicionesEncontrado

        } = this.state;

        if (nombre.length === 0) {
            alert('Falta el posible nombre');
            return;
        }
        if (apodo.length === 0) {
            alert('Falta el apodo');
            return;
        }
        if (documentoIdentidad.length === 0) {
            alert('Falta cedula de ciudadania');
            return;
        }

        if (trasladoDe.length === 0) {
            alert('Falta trasladado desde');
            return;
        }
        if (lugarAcogida.length === 0) {
            alert('Falta el lugar de acogida');
            return;
        }
        if (referenciasFamiliares.length === 0) {
            alert('Faltan las referencias familiares');
            return;
        }
        if (condicionEntrontrado.length === '') {
            alert('Faltan las condiciones de encuentro');
            return;
        }
        if (dondeFueEncontrado.length === 0) {
            alert('Falta el lugar de encuentro');
            return;
        }

        // if (idEdadAparente === undefined) {
        //     alert('Falta la edad aparente');
        //     return;
        // }
        if (edad.length === 0) {
            alert('Falta la edad');
            return;
        }
        if (estatura.length === 0) {
            alert('Falta la estatura');
            return;
        }
        if (seniales.length === 0) {
            alert('Faltan las señales particulares');
            return;
        }
        if (idSexo === undefined) {
            alert('Falta el sexo');
            return;
        }
        if (idColorcabello === undefined) {
            alert('Falta el color del cabello');
            return;
        }
        if (idContextura === undefined) {
            alert('Falta la contextura');
            return;
        }
        if (idColorpiel === undefined) {
            alert('Falta el color de la piel');
            return;
        }
        if (idColorojos === undefined) {
            alert('Falta el color de los ojos');
            return;
        }
        if (idDiscapacidad === undefined) {
            alert('Faltan los Datos Complementario de Discapacidad');
            return;
        }
        if (idSenial === undefined) {
            alert('Falta las Señales Particulares');
            return;
        }
        if (foto === null) {
            alert('Falta la Foto');
            return;
        }

        this.setState({dialogVisible: true});

        this.getCurrentLocation();
        const { navigation } = this.props;
        let token =  navigation.getParam('token');
        let usuario =  navigation.getParam('usuario');

        console.log('token',token);
        // token = 'chXL+4rhYu2I5dRb8ThPXA==0749cb5aee9a6edc48ea75981a12112811f852a6f57c2a0741794253bde39788';
        this.setState({token: token});
        this.setState({usuario: usuario});


        let bodyForm = {
            nombre: nombre,
            edad:edad,
            // edad:idEdadAparente,
            idSexo: idSexo,
            estatura: estatura,
            apodo: apodo,
            trasladadoDe: trasladoDe,
            lugarAcogida: lugarAcogida,
            referenciasFamiliares: referenciasFamiliares,
            idColorcabello: idColorcabello,
            idColorpiel: idColorpiel,
            idColorojos: idColorojos,
            idDiscapacidad: idDiscapacidad,
            usuario: usuario,
            latitud: this.state.mapRegion.latitude,
            longitud: this.state.mapRegion.longitude,
            idContextura: idContextura,
            foto: foto,
            seniales: [
                {
                    idSenial: idSenial,
                    senial: seniales,
                    observacion: seniales
                }
            ],


            documentoIdentidad: documentoIdentidad,
            dondeFueEncontrado: dondeFueEncontrado,
            condicionEncontrado: condicionEntrontrado
        };


        console.log('condicionesEncontradoA',condicionesEncontradoA);

        //     idCondicionesEncontrado: idCondicionesEncontrado,

        //     idEdadAparente:idEdadAparente,

        let urlRegister =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/desaparecido/registronn';

        console.log('token', this.state.token);
        console.log('envio', bodyForm);

        const options = {
            url: urlRegister,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'token-api': token
            },
            data: bodyForm
        };

        axios(options)
            .then(response => {

                console.log('ok');
                if (response.status=== 201){
                    this.setState({dialogVisible: false});
                    this.setState({dialogoRegistro: true});
                    // alert('registro correcto');
                    this.setState({nombre : ''});
                    this.setState({edad : ''});
                    this.setState({estatura : ''});
                    this.setState({apodo : ''});
                    this.setState({trasladoDe : ''});
                    this.setState({lugarAcogida : ''});
                    this.setState({referenciasFamiliares : ''});
                    this.setState({seniales : ''});
                    this.setState({mapRegion : ['','']});

                    this.setState({idSexo : undefined});
                    this.setState({idColorcabello : undefined});
                    this.setState({idColorpiel : undefined});
                    this.setState({idColorojos : undefined});
                    this.setState({idDiscapacidad : undefined});
                    this.setState({idContextura : undefined});
                    this.setState({idSenial : undefined});


                    this.setState({sexoA : null});
                    this.setState({colorCabelloA : null});
                    this.setState({colorPielA : null});
                    this.setState({colorOjosA : null});
                    this.setState({discapacidadA : null});
                    this.setState({senialA : null});
                    this.setState({contexturaA : null});

                    this.setState({foto : null});

                    this.setState({documentoIdentidad : ''});
                    this.setState({condicionEncontrado : null});
                    this.setState({dondeFueEncontrado : ''});
                    // this.setState({edadAparente : null});

                    this.setState({condicionesEncontradoA : null});

                }

            })
            .catch(error => {
                console.log(error);
                this.setState({dialogVisible: false});
                alert('Error de registro, por favor comprueba tu conexión a internet');


            });
    };

    onPictureSaved = async photo => {
        this.setState({photo : photo.uri});


        let status2 = await Permissions.getAsync(Permissions.LOCATION);
        if (status2 !== "granted") {
          alert('Por favor, acepta los permisos cuando lo requiera para su correcto funcionamiento');
          const { status } = await Permissions.askAsync(Permissions.LOCATION);
        }

        console.log(photo.uri);

        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === "granted") {

            const asset = await MediaLibrary.createAssetAsync(photo.uri);
            console.log(asset);

            await MediaLibrary.createAlbumAsync("Ministerio/fotos", asset, false)


        }

    

        }

    getCatalogCabello = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/colorcabello';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ colorCabello: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.colorCabelloT.push(datos[i].descripcion);
            this.state.colorCabelloI.push(datos[i].idcolorcabello);
        }

    };
    getCatalogContextura = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/contextura';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ contextura: datos });
        console.log(this.state.contextura);
        let i;
        for (i=0; i<datos.length; i++){
            this.state.contexturaT.push(datos[i].descripcion.slice(0, datos[i].descripcion.indexOf('(')-1));
            this.state.contexturaI.push(datos[i].idcontextura);
        }



        console.log(this.state.contexturaT);
        // console.log(this.state.contexturaI);
    };
    getCatalogOjos = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/colorojos';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ colorOjos: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.colorOjosT.push(datos[i].descripcion);
            this.state.colorOjosI.push(datos[i].idcolorojos);
        }

    };
    getCatalogSexo = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/sexo';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ sexo: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.sexoT.push(datos[i].descripcion);
            this.state.sexoI.push(datos[i].idsexo);
        }

    };
    getCatalogPiel = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/colorpiel';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ colorPiel: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.colorPielT.push(datos[i].descripcion);
            this.state.colorPielI.push(datos[i].idcolorpiel);
        }

    };
    getCatalogDiscapacidad = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/discapacidad';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ discapacidad: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.discapacidadT.push(datos[i].descripcion);
            this.state.discapacidadI.push(datos[i].iddiscapacidad);
        }
    };
    getCatalogSenial = async token => {
        const url =
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/senial';
        const { data } = await axios.get(url, {
            headers: { 'token-api': token, 'content-type': 'application/json' }
        });
        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ senial: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.senialT.push(datos[i].descripcion);
            this.state.senialI.push(datos[i].idsenial);
        }

        console.log(datos);

    };

    getCatalogCondicionesEncontrado = async token => {
        // const url =
        //     'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/senial';
        // const { data } = await axios.get(url, {
        //     headers: { 'token-api': token, 'content-type': 'application/json' }
        // });
        // let datos = JSON.parse(JSON.stringify(data));

        let datos = [
            {
                descripcion: 'Orientado',
                idcondicionesEncontrado: 1,
            },
            {
                descripcion: 'Desorientado',
                idcondicionesEncontrado: 2,
            },
            {
                descripcion: 'Con efectos de consumo de droga',
                idcondicionesEncontrado: 3,
            },
            {
                descripcion: 'Con secuelas de violencia',
                idcondicionesEncontrado: 4,
            }
        ];

        this.setState({ condicionesEncontrado: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.condicionesEncontradoT.push(datos[i].descripcion);
            this.state.condicionesEncontradoI.push(datos[i].idcondicionesEncontrado);
        }


    };
    getCatalogEdadAparente = async token => {
        // const url =
        //     'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/catalogo/senial';
        // const { data } = await axios.get(url, {
        //     headers: { 'token-api': token, 'content-type': 'application/json' }
        // });
        // let datos = JSON.parse(JSON.stringify(data));

        let datos = [
            {
                descripcion: '0 a 11 años',
                idedadAparente: 1,
            },
            {
                descripcion: '12 a 17 años',
                idedadAparente: 2,
            },
            {
                descripcion: '18 a 29 años',
                idedadAparente: 3,
            },
            {
                descripcion: '30 a 64 años',
                idedadAparente: 4,
            },
            {
                descripcion: '65 - en adelante',
                idedadAparente: 5,
            }
        ];

        this.setState({ edadAparente: datos });
        let i;
        for (i=0; i<datos.length; i++){
            this.state.edadAparenteT.push(datos[i].descripcion);
            this.state.edadAparenteI.push(datos[i].idedadAparente);
        }


    };





    async componentDidMount() {
        this.getCurrentLocation();
        const { navigation } = this.props;
        let token = navigation.getParam('token');
        let usuario = navigation.getParam('usuario');

        console.log('token', token);
        // token = 'chXL+4rhYu2I5dRb8ThPXA==0749cb5aee9a6edc48ea75981a12112811f852a6f57c2a0741794253bde39788';
        this.setState({ token: token });
        this.setState({ usuario: usuario });

        this.getCatalogCabello(token);
        this.getCatalogContextura(token);
        this.getCatalogSexo(token);
        this.getCatalogPiel(token);
        this.getCatalogDiscapacidad(token);
        this.getCatalogSenial(token);
        this.getCatalogOjos(token);


        this.getCatalogCondicionesEncontrado(token);
        this.getCatalogEdadAparente(token);



    }

    showCamera() {
        this.setState({ cameraVisible: true });
    }

    loadPicture(image) {
        this.setState({ cameraVisible: false, foto: image['base64'] });
    }



    render() {

        return (
            <View style={styles.container}>
                <Dialog
                    visible={this.state.dialogoRegistro}
                    title="REGISTRO CORRECTO"
                    onTouchOutside={() => this.setState({dialogoRegistro: false})} >
                    {/*<View>*/}
                    {/*    // your content here*/}
                    {/*</View>*/}
                </Dialog>
                <ProgressDialog
                    visible={this.state.dialogVisible}
                    style={styles.estiloDialogWaiting}
                    title="SUBIENDO INFORMACIÓN,"
                    message="POR FAVOR ESPERE UN MOMENTO."
                />
                {/*<Dialog*/}
                {/*    visible={this.state.dialogVisible}*/}
                {/*    title="Custom Dialog"*/}
                {/*    onTouchOutside={() => this.setState({dialogVisible: false})} >*/}
                {/*    <View>*/}
                {/*        /!*<text>hola</text>*!/*/}
                {/*    </View>*/}
                {/*</Dialog>*/}
                <MenuButton navigation={this.props.navigation} />
                <View style={styles.headerActions}>
                    <TouchableOpacity
                        style={styles.actionButton}
                        onPress={() => this.goToUserLogin()}
                    ></TouchableOpacity>
                </View>
                <KeyboardAvoidingView
                    style={styles.userRegistrationForm}
                    behavior="height"
                    keyboardVerticalOffset={60}
                >
                    <ScrollView style={styles.scroll}>
                        <Text style={styles.formHeaders}>Datos Generales</Text>
                        <TextInput
                            style={styles.formInputs}
                            placeholder="Posible Nombre"
                            value={this.state.nombre}
                            onChange={nombre => {
                                console.log(nombre.nativeEvent.text);
                                this.setState({ nombre: nombre.nativeEvent.text });
                            }}
                        />
                        <TextInput
                            style={styles.formInputs}
                            placeholder="Apodo"
                            value={this.state.apodo}
                            onChange={apodo => this.setState({ apodo: apodo.nativeEvent.text })}
                        />

                        <TextInput
                            style={styles.formInputs}
                            placeholder="Cédula de ciudadania"
                            value={this.state.documentoIdentidad}
                            onChange={variable => this.setState({ documentoIdentidad: variable.nativeEvent.text })}
                        />

                        <TextInput
                            style={styles.formInputs}
                            placeholder="Fue trasladado(a) desde"
                            value={this.state.trasladoDe}
                            onChange={trasladoDe => this.setState({ trasladoDe: trasladoDe.nativeEvent.text })}
                        />
                        <TextInput
                            style={styles.formInputs}
                            placeholder="Se encuentra en"
                            value={this.state.lugarAcogida}
                            onChange={lugarAcogida =>
                                this.setState({ lugarAcogida: lugarAcogida.nativeEvent.text })
                            }
                        />
                        <TextInput
                            style={styles.formInputs}
                            placeholder="Referencias Familiares"
                            value={this.state.referenciasFamiliares}
                            onChange={referenciasFamiliares =>
                                this.setState({ referenciasFamiliares: referenciasFamiliares.nativeEvent.text })
                            }
                        />
                        <TextInput
                            style={styles.formInputs}
                            placeholder="Fue encontrado en:"
                            value={this.state.dondeFueEncontrado}
                            onChange={variable =>
                                this.setState({ dondeFueEncontrado: variable.nativeEvent.text })
                            }
                        />

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetCondicionesEncontrado.show()
                                }}
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                    title='Condiciones encontrado:' />
                            <ActionSheet
                                ref={o => this.ActionSheetCondicionesEncontrado = o}
                                title={'Condiciones encontrado:'}
                                options={this.state.condicionesEncontradoT}
                                onPress={(index) => { this.setState({condicionesEncontradoA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.condicionesEncontradoT[this.state.condicionesEncontradoA]}</Text>
                        </View>


                        {/*<View style={styles.buttonSize}>*/}
                        {/*    <Button onPress={*/}
                        {/*        ()=> {*/}
                        {/*            // this.ActionSheetCondicionesEncontrado.show()*/}
                        {/*        }}*/}
                        {/*            type="outline"*/}
                        {/*            raised={true}*/}
                        {/*            titleStyle={styles.buttonText}*/}
                        {/*            buttonStyle={{*/}
                        {/*                backgroundColor: "white",*/}
                        {/*                flex: 1,*/}
                        {/*            }}*/}
                        {/*            title='Condiciones encontrado:' />*/}
                        {/*    <ActionSheet*/}
                        {/*        ref={o => this.ActionSheetCondicionesEncontrado = o}*/}
                        {/*        title={'Condiciones encontrado:'}*/}
                        {/*        options={this.state.condicionesEncontrado}*/}
                        {/*        onPress={(index) => { this.setState({condicionesEncontrado: index});*/}
                        {/*        }}*/}
                        {/*    />*/}
                        {/*    <Text style={styles.buttonText}>  {this.state.condicionesEncontradoT[this.state.condicionesEncontradoA]}</Text>*/}
                        {/*</View>*/}




                        <Text style={styles.formHeaders}>Datos Físicos</Text>

                        <TextInput
                            style={styles.formInputs}
                            placeholder="Edad aparente"
                            value={this.state.edad}
                            onChange={edad => this.setState({ edad: edad.nativeEvent.text })}
                            keyboardType="phone-pad"
                        />


                        {/*<View style={styles.buttonSize}>*/}
                        {/*    <Button onPress={*/}
                        {/*        ()=> {this.ActionSheetEdadAparente.show()*/}
                        {/*        }}*/}
                        {/*            type="outline"*/}
                        {/*            raised={true}*/}
                        {/*            titleStyle={styles.buttonText}*/}
                        {/*            buttonStyle={{*/}
                        {/*                backgroundColor: "white",*/}
                        {/*                flex: 1,*/}
                        {/*            }}*/}
                        {/*            title='Edad aparente:' />*/}
                        {/*    <ActionSheet*/}
                        {/*        ref={o => this.ActionSheetEdadAparente = o}*/}
                        {/*        title={'Edad aparente:'}*/}
                        {/*        options={this.state.edadAparenteT}*/}
                        {/*        onPress={(index) => { this.setState({edadAparenteA: index});*/}
                        {/*        }}*/}
                        {/*    />*/}
                        {/*    <Text style={styles.buttonText}>  {this.state.edadAparenteT[this.state.edadAparenteA]}</Text>*/}
                        {/*</View>*/}


                        <TextInput
                            style={styles.formInputs}
                            placeholder="Estatura (cm)"
                            value={this.state.estatura}
                            onChange={estatura => this.setState({ estatura: estatura.nativeEvent.text })}
                            keyboardType="phone-pad"
                        />

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetContextura.show()
                            }}
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                title='Contextura:' />
                            <ActionSheet
                                ref={o => this.ActionSheetContextura = o}
                                title={'Contextura:'}
                                options={this.state.contexturaT}
                                onPress={(index) => { this.setState({contexturaA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.contexturaT[this.state.contexturaA]}</Text>
                        </View>

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetSexo.show()
                                }}
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                title='Sexo:' />
                            <ActionSheet
                                ref={o => this.ActionSheetSexo = o}
                                title={'Sexo:'}
                                options={this.state.sexoT}
                                onPress={(index) => { this.setState({sexoA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.sexoT[this.state.sexoA]}</Text>
                        </View>

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetCabello.show()
                            }}

                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                title='Cabello:' />
                            <ActionSheet
                                ref={o => this.ActionSheetCabello = o}
                                title={'Cabello:'}
                                options={this.state.colorCabelloT}
                                onPress={(index) => { this.setState({colorCabelloA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.colorCabelloT[this.state.colorCabelloA]}</Text>
                        </View>

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetColorPiel.show()}
                            }
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                title='Color de Piel:' />
                            <ActionSheet
                                ref={o => this.ActionSheetColorPiel = o}
                                title={'Color de Piel:'}
                                options={this.state.colorPielT}
                                onPress={(index) => { this.setState({colorPielA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.colorPielT[this.state.colorPielA]}</Text>
                        </View>

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetColorOjos.show()}
                            }
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                title='Color de ojos:' />
                            <ActionSheet
                                ref={o => this.ActionSheetColorOjos = o}
                                title={'Color de ojos:'}
                                options={this.state.colorOjosT}
                                onPress={(index) => { this.setState({colorOjosA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.colorOjosT[this.state.colorOjosA]}</Text>
                        </View>


                        <Text style={styles.formHeaders}>Datos Complementarios</Text>

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetDiscapacidad.show()}
                            }
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                    title='Seleccione Discapacidad:' />
                            <ActionSheet
                                ref={o => this.ActionSheetDiscapacidad = o}
                                title={'Seleccione Discapacidad:'}
                                options={this.state.discapacidadT}
                                onPress={(index) => { this.setState({discapacidadA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.discapacidadT[this.state.discapacidadA]}</Text>
                        </View>



                        <Text style={styles.formHeaders}>Señales Particulares</Text>

                        <View style={styles.buttonSize}>
                            <Button onPress={
                                ()=> {this.ActionSheetSenial.show()}
                            }
                                    type="outline"
                                    raised={true}
                                    titleStyle={styles.buttonText}
                                    buttonStyle={{
                                        backgroundColor: "white",
                                        flex: 1,
                                    }}
                                    title='Seleccione Señal:' />
                            <ActionSheet
                                ref={o => this.ActionSheetSenial = o}
                                title={'Seleccione Señal:'}
                                options={this.state.senialT}
                                onPress={(index) => { this.setState({senialA: index});
                                }}
                            />
                            <Text style={styles.buttonText}>  {this.state.senialT[this.state.senialA]}</Text>
                        </View>


                        {/*<View>*/}
                        {/*    <Text>Seleccione Señal:</Text>*/}
                        {/*    <Picker*/}
                        {/*        selectedValue={this.state.idSenial}*/}
                        {/*        style={styles.picker}*/}
                        {/*        onValueChange={(itemValue, itemIndex) => this.setState({ idSenial: itemValue })}*/}
                        {/*    >*/}
                        {/*        {this.state.senial.map(v => {*/}
                        {/*            return <Picker.Item label={v.descripcion} value={v.idsenial}  key={v.idsenial} />;*/}
                        {/*        })}*/}
                        {/*    </Picker>*/}
                        {/*</View>*/}
                        <TextInput
                            style={styles.formInputs}
                            placeholder="Donde se ubica la señal"
                            value={this.state.seniales}
                            onChange={seniales => this.setState({ seniales: seniales.nativeEvent.text })}
                            keyboardType="email-address"
                        />

                        <Text style={styles.formHeaders}>Fotografía</Text>
                        <TouchableOpacity
                            onPress={() => this.showCamera()}
                            style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 20 }}
                        >
                            {this.state.foto === null ? (
                                <Image
                                    source={require('../../assets/home/camara.png')}
                                    style={styles.ImagesView}
                                    blurRadius={1}
                                />
                            ) : (
                                <Image
                                    style={{ width: 200, height: 200, resizeMode: 'contain' }}
                                    source={{ uri: `data:image/gif;base64,${this.state.foto}` }}
                                />
                            )}
                        </TouchableOpacity>

                        <View style={styles.buttonContainer}>
                            <Button
                                buttonType="outline"
                                onPress={this.registerNN}
                                title="REGISTRAR"
                                buttonColor="#fff"
                                backgroundColor="#053862"
                            />
                        </View>

                        {/*<View style={styles.buttonContainer}>*/}
                        {/*    <FormButton*/}
                        {/*        buttonType="outline"*/}
                        {/*        onPress={this.registerNN}*/}
                        {/*        title="REGISTRAR"*/}
                        {/*        buttonColor="#039BE5"*/}
                        {/*    />*/}
                        {/*</View>*/}

                        <Modal animationType="slide" transparent={false} visible={this.state.cameraVisible}>
                            <TouchableHighlight
                                onPress={() => {
                                    this.setState({ cameraVisible: false });
                                }}
                                style={{ position: 'absolute', top: 20, right: 20, zIndex: 1 }}
                            >
                                <FontAwesome name="close" size={40} color="#fff" />
                            </TouchableHighlight>
                            <CameraNN loadPicture={image => this.loadPicture(image)} />
                        </Modal>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scroll: {},
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    headerActions: {
        flexDirection: 'row',
        height: 60,
        width: '100%',
        alignSelf: 'flex-start',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginTop: Constants.statusBarHeight
    },
    actionButton: {
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    actionText: {
        marginLeft: 10,
        fontWeight: '600',
        color: '#053862',
        fontSize: 14
    },
    userRegistrationForm: {
        display: 'flex',
        flex: 1,
        width: '95%',
        flexDirection: 'column'
    },
    formHeaders: {
        width: '100%',
        height: 40,
        // backgroundColor: '#ECECEC',
        backgroundColor: '#00ACBF',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 16,
        // color: '#053862',
        color: '#fff',
        borderColor: '#A5A5A5',
        borderRadius: 5
    },
    formInputs: {
        borderColor: '#ECECEC',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 5,
        height: 30,
        marginHorizontal: 20,
        marginVertical: 10
    },
    picker: {
        height: 50,
        width: 200,
        alignSelf: 'center'
    },
    buttonContainer:{
        width: (Dimensions.get('window').width * 0.75) , // approximate a square
        alignSelf: 'center'
    },

    ImagesView: {
        // position: 'absolute',
        height: (Dimensions.get('window').width - 10) / 2, // approximate a square
        width: (Dimensions.get('window').width - 10) / 2, // approximate a square
        alignSelf: 'center'

        // borderRadius: 5
    },
    buttonSize:{
        // width: '30%'
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5,
    },
    buttonText:{
        fontSize: (Dimensions.get('window').width /30) , // approximate a square
        color: '#074d57'
    }, estiloDialogWaiting:{


    }
});

