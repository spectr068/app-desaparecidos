import React from 'react';
import { Modal, Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MenuButton from '../components/MenuButton';
import Constants from 'expo-constants';
import { FlatList } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';
import axios from 'axios';
import MapView, { Marker } from 'react-native-maps';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import UnknownPeopleRegistration from "./UnknownPeopleRegistration";
import MissingPeople from "./MissingPeople";
import FindingFamily from "./FindingFamily";
import Places from "./Places";
//import { Ionicons } from '@expo/vector-icons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Consejos from "./Consejos";
import {MenuProvider} from "react-native-popup-menu";
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';


function HomeScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' ,       backgroundColor: '#00ACBF',
        }}>
            <Image source={gifHome.image} style={styles.ImagesView} />

            {/*<Text>Home!</Text>*/}
        </View>
    );
}

function SettingsScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings! imagen_presentacion</Text>
        </View>
    );
}

const Tab = createBottomTabNavigator();

export default class Home extends React.Component {
  state = {
    modalVisible: false,
    emilyAlerts: []
  };

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.moduleView}
        onPress={() => this.props.navigation.navigate(item.screen)}
      >
        <Image source={item.image} style={styles.ImagesView} blurRadius={1} />
        <Text style={styles.moduleName}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  getEmilyAlerts = async (url) => {
    try {
      const {data} = await axios.get( url );
        let datos = JSON.parse(JSON.stringify(data));
        let visible = false;
      if (datos.length !== 0){
          visible = true;
      }

      console.log(datos);
      this.setState({ emilyAlerts: datos, modalVisible: visible });
    } catch (error) {
      console.error(error);
    }
  };
  fun(){

  }

  componentDidMount() {

    // const { status } = await Permissions.askAsync(Permissions.LOCATION);
    // if (status !== 'granted') {
    //   console.log('Permission to access location was denied');
    //   alert('Permission to access location was denied');
    // } else {
    //   console.log('Permission to access location granted');
    //   alert('Permission to access location granted');
    // }


    this.checkPermisos();


    const url = 'http://52.43.113.195:8080/TEST/ministerio/api/test/alertaEmilia';
    // this.getEmilyAlerts(url);
  }

  checkPermisos = async () =>{
    

    // const { status } = await Permissions.askAsync(Permissions.LOCATION);
    // let { status } = await Location.requestPermissionsAsync();
    let {status} = await Permissions.getAsync(Permissions.LOCATION);
    if (status !== "granted") {
      alert('Por favor, acepta los permisos cuando lo requiera para su correcto funcionamiento');
    }

    // if (status !== 'granted') {
    //   console.log('Permission to access location was denied');
    //   let permisos = await Permissions.askAsync(Permissions.LOCATION);

      // alert('Permission to access location was denied');
    // } else {
    //   console.log('Permission to access location granted');
    //   alert('Permission to access location granted');
    // }

  }





  render() {
    return (

        <NavigationContainer
             style={styles.container}
        >
            {/*<MenuProvider style={styles.test}>*/}
                <MenuButton navigation={this.props.navigation} />
            {/*</MenuProvider>*/}
            <View style={styles.headerView}>
                <Image style={styles.headerView_logo1} source={require('../assets/logo2.png')} />
                <View style={styles.ViewLogo2}>
                    <Image style={styles.headerView_logo2} source={require('../assets/main-logo.png')} />
                </View>
            </View>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;

                        if (route.name === 'Home') {
                            iconName = focused
                                ? 'ios-home'
                                : 'ios-home';
                        } else if (route.name === 'Desaparecidos') {
                            iconName = focused
                                ? 'ios-search'
                                : 'ios-search';
                        } else if (route.name === 'Reencuentro') {
                            iconName = focused
                                ? 'ios-people'
                                : 'ios-people';
                        } else if (route.name === 'Consejos') {
                            iconName = focused
                                ? 'ios-list-box'
                                : 'ios-list-box';
                        }
                        else if (route.name === 'Donde denunciar') {
                            iconName = focused
                                ? 'ios-pin'
                                : 'ios-pin';
                        }

                        // You can return any component that you like here!
                        return <Ionicons name={iconName} size={size} color={color} />;
                    },
                })}
                tabBarOptions={{
                    // activeTintColor: '#00ACBF',
                    activeTintColor: '#074d57',
                    inactiveTintColor: 'gray',
                    activeBackgroundColor: '#00ACBF',
                }}
            >



                <Tab.Screen name="Home" component={HomeScreen} />
                <Tab.Screen name="Consejos" component={Consejos} />
                <Tab.Screen name="Desaparecidos" component={MissingPeople} />

                <Tab.Screen name="Reencuentro" component={FindingFamily} />


                <Tab.Screen name="Donde denunciar" component={Places} />






            </Tab.Navigator>
         </NavigationContainer>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00ACBF',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  headerView: {
    width: '75%',
    height: 80,
    // alignSelf: 'center',
    marginTop: Constants.statusBarHeight,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
  },
  headerView_logo1: {
      flex:1,
    width: '30%',
    height: undefined,
      aspectRatio: 2,
      justifyContent: 'center',
      alignItems: 'center',

  },
  ViewLogo2:{
   flex:2,
   height: 80,
//   backgroundColor: '#0000FF',
   alignItems: 'center'
  },
  headerView_logo2: {
    height:'100%',
    width : undefined,
//    width: '50%',
	aspectRatio: 2.5,
 //   backgroundColor: '#ff0000'

  },
  modulesContainer: {
    flex: 1,
    width: '100%',
    marginVertical: 20
  },
  moduleView: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    marginHorizontal: 5,
    marginVertical: 5,
    height: (Dimensions.get('window').height - 80 - 60 - 10 * 5) / 5 // approximate a square
  },
  ImagesView: {
      backgroundColor: '#00ACBF',

      // position: 'absolute',
    // height: '50%', // approximate a square
    // width: '50%', // approximate a square
    height: undefined, // approximate a square
    width: '100%', // approximate a square
      aspectRatio: 0.94,

      // borderRadius: 5
  },
  moduleName: {
    fontSize: 14,
    textAlign: 'center',
    color: '#fff',
    alignSelf: 'center'
  },
  footerView: {
    margin: 10,
    height: 60,
    width: '100%',
    alignSelf: 'stretch'
  },
  footerView_logo: {
    width: '100%',
    height: '100%'
  },
  modalInfo: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    flex: 1
  },
  modalInfoCloseButtonContainer: {
    flex: 1,
    height: 20,
    alignItems: 'flex-end'
  },
  infoContainer: {
    flex: 8,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mapStyle: {
    width: Dimensions.get('window').width * 0.9,
    height: Dimensions.get('window').height * 0.5
  },
    test:{
        backgroundColor: '#ff0000',
        // position: 'absolute',
        marginTop: Constants.statusBarHeight,
        top: 0,
        right: 0,
        width: '100%',
        height: 50,

    }
});


const gifHome = {
    key: 1,
    name: 'Información',
    screen: 'Info',
    // image: require('../assets/home/desaparecidos_home.gif')
    image: require('../assets/home/home.jpg')
};


const modules = [
  {
    key: 1,
    name: 'Información',
    screen: 'Info',
    image: require('../assets/home/fondoBoton.png')
  },
  {
    key: 2,
    name: 'Lugares donde denunciar',
    screen: 'Places',
    image: require('../assets/home/fondoBoton.png')
  },
  {
    key: 3,
    name: 'Personas desaparecidas',
    screen: 'MissingPeople',
    image: require('../assets/home/fondoBoton.png')
  },
  {
    key: 4,
    name: 'Busco a mis familiares',
    screen: 'FindingFamily',
    image: require('../assets/home/fondoBoton.png')
  },
  {
    key: 5,
    name: 'Casos resueltos',
    screen: 'SolvedCases',
    image: require('../assets/home/fondoBoton.png')
  },
  {
    key: 6,
    name: 'Campañas',
    screen: 'Campaigns',
    image: require('../assets/home/fondoBoton.png')
  },
  {
    key: 7,
    name: 'Alerta Emilia',
    screen: 'EmilyAlert',
    image: require('../assets/home/fondoBoton.png')
  }
  // ,
  // {
  //   key: 8,
  //   name: 'Registro de personas desconocidas',
  //   screen: 'UnknownPeopleRegistration',
  //     image: require('../assets/home/fondoBoton.png')

  // }
];
