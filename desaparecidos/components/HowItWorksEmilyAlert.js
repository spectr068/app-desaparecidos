import React, { Fragment } from 'react';
import { Image, StyleSheet, Text } from 'react-native';

export default class HowItWorksEmilyAlert extends React.Component {
  render() {
    return (
      <Fragment>
        <Text style={styles.presentationText}>
          Una vez que las entidades encargadas determinan que un niño, niña o adolescente ha
          desaparecido, se encuentra en alto riesgo y cumple con los criterios de Alerta EMILIA, se
          activa inmediatamente su alarma y la notificación de búsqueda mediante Facebook 160 km a
          la redonda de la desaparición
        </Text>
        <Image
          style={{ width: '100%', height: 250 }}
          source={require('../assets/diagrama-alerta-emilia.jpg')}
          resizeMode="contain"
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  presentationText: {
    color: '#053862',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify'
  }
});
