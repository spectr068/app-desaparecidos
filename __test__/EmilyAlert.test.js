import React from 'react';
import EmilyAlert from '../screens/EmilyAlert';

import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<EmilyAlert />).toJSON();
  expect(tree).toMatchSnapshot();
});
