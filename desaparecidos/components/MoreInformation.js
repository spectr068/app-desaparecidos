import React, { Fragment } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class MoreInformation extends React.Component {
  render() {
    return (
      <Fragment>
        <View style={styles.infoContainer}>
          <Text style={styles.infoHeader}>01</Text>
          <View style={styles.infoTextContainer}>
            <Text style={styles.presentationTextHeader}>Persona perdida:</Text>
            <Text style={styles.presentationText}>
              Tomando la definición del Código de la Niñez y Adolescencia; se considera persona
              perdida a las niñas, niños, adolescentes, cuya ausencia voluntaria o involuntaria del
              hogar, establecimiento educativo u otro lugar en el que se supone deben permanecer,
              sin el consentimiento o conocimiento de sus progenitores o responsables de su cuidado.
            </Text>
          </View>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoHeader}>02</Text>
          <View style={styles.infoTextContainer}>
            <Text style={styles.presentationTextHeader}>Persona extraviada:</Text>
            <Text style={styles.presentationText}>
              Se considera como persona extraviada aquella que luego de salir a su domicilio o de
              algún otro lugar, y por causas ajenas a su voluntad no puede regresar.
            </Text>
          </View>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoHeader}>03</Text>
          <View style={styles.infoTextContainer}>
            <Text style={styles.presentationTextHeader}>Persona Desaparecida:</Text>
            <Text style={styles.presentationText}>
              Se considera como persona desaparecida a toda persona que se encuentre en paradero
              desconocido para sus familiares, amigos y otras personas cercanas.
            </Text>
          </View>
        </View>
        <View style={styles.infoImageContainer}>
          <Image
            style={styles.instructionImage}
            source={require('../assets/imagen-perdida-extraviada.png')}
            resizeMode="contain"
          />
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoHeader}>04</Text>
          <View style={styles.infoTextContainer}>
            <Text style={styles.presentationTextHeader}>
              ¿Por qué las personas se pierden o extravían voluntariamente?
            </Text>
            <Text style={styles.presentationText}>
              Según datos de la Dirección Nacional de Delitos contra la Vida, Muertes Violentas,
              Desapariciones, Extorsión y Secuestros (DINASED), las principales razones por las que
              las personas deciden de manera voluntaria ausentarse y no informar de su paradero en
              el Ecuador son aquellas que afectan su entorno o contexto en el cual el individuo se
              desarrolla Por ejemplo, violencia intrafamiliar, bajo rendimiento académico, maltrato
              escolar, deudas, consumo de drogas y alcohol, vinculación con pandillas, entre otros.
            </Text>
          </View>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoHeader}>05</Text>
          <View style={styles.infoTextContainer}>
            <Text style={styles.presentationTextHeader}>
              ¿Una persona puede estar perdida o extraviada involuntariamente?
            </Text>
            <Text style={styles.presentationText}>
              Si, existen situaciones o circunstancias que no pueden ser controladas o modificadas
              por la persona desaparecida como por ejemplo: accidentes, desastres naturales,
              incidentes que tengan relación con un delito o ser víctima de un delito. En estos
              escenarios, la desaparición de una persona puede darse de modo involuntario. Por
              motivos involuntarios como los relacionados con su estado de salud mental tales como
              autismo, asperger, bipolaridad, depresión, demencias, Alzheimer, desorientación u
              otras enfermedades o trastornos mentales.
            </Text>
          </View>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.infoHeader}>06</Text>
          <View style={styles.infoTextContainer}>
            <Text style={styles.presentationTextHeader}>¿Qué es una desaparición forzada?</Text>
            <Text style={styles.presentationText}>
              La desaparición forzada se produce cuando un agente del Estado o quien actúe con su
              consentimiento, por cualquier medio, someta a privación de libertad a una persona,
              seguida de la falta de información o de la negativa a reconocer dicha privación de
              libertad o de informar sobre el paradero o destino de una persona, con lo cual se
              impida el ejercicio de garantías constitucionales o legales. Este delito es sancionado
              con pena privativa de libertad de 22 a 26 años.
            </Text>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  infoTextContainer: {
    flex: 9,
    color: '#fff',
    fontSize: 16,
    margin: 5,
    marginLeft: 10,
    textAlign: 'justify'
  },
  infoContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10
  },
  infoHeader: {
    flex: 1,
    color: '#0099D0',
    backgroundColor: '#fff',
    borderRadius: 10,
    height: 40,
    width: 40,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  infoImageContainer: {
    maxHeight: 250,
    maxWidth: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  instructionImage: {
    flex: 1
  },
  presentationTextHeader: {
    color: '#fff',
    fontSize: 20,
    margin: 5,
    textAlign: 'left',
    fontWeight: '600'
  },
  presentationText: {
    color: '#fff',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify'
  }
});
