import React from 'react';
import {Dimensions, StyleSheet, View, Image, Text} from 'react-native';
import MenuButton from '../components/MenuButton';
import MapView, { Marker } from 'react-native-maps';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import axios from 'axios';
import Constants from "expo-constants";

export default class Places extends React.Component {
  state = {
    mapRegion: {
      latitude: -0.1765486,
      longitude: -78.4831857,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0621
    },
    hasLocationPermissions: false,
    locationResult: null,
      markersUVC: [],
      markersUPC: [],
      markersCasasAcogida: [],
  };

  componentDidMount() {
    this.getCurrentLocation();
    // console.log(location);
  }

    async getUPC (location) {

      console.log(location);

        const { data } = await axios.get(
            'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/busqueda/upcs/distancia/5/' +
            location.coords.latitude +
            '/' +
            location.coords.longitude
        );


        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ markersUPC: [] });
        let marcadoresUPC = [];
        let i;

        console.log(datos);

        for (i = 0; i < datos.length; i++) {
            let aux;

            if (datos[i]['tipo'] === 'UPC') {
                aux = {
                    title: datos[i]['nombrepoblacion'],
                    description: datos[i]['tipo'] + ' - ' + datos[i]['telefono']
                };
                aux.location = { latitude: datos[i]['latitud'], longitude: datos[i]['longitud'] };
                aux.image = require('../assets/policia.png');
                aux.anchor = { x: 0.125, y: 0.1 };
                marcadoresUPC.push(aux);

            }
            // if (datos[i]['tipo'] === 'UVC') {
            //     aux = {
            //         title: datos[i]['nombrepoblacion'],
            //         description: datos[i]['tipo'] + ' - ' + datos[i]['telefono']
            //     };
            //     aux.location = { latitude: datos[i]['latitud'], longitude: datos[i]['longitud'] };
            //     aux.image = require('../assets/encontrado.png');
            //     aux.anchor = { x: 0.125, y: 0.1 };
            //     // marcadoresUVC.push(aux);
            //
            // }

        }
        this.setState({ markersUPC: marcadoresUPC });
  }
    async getUVC (location) {
        const { data } = await axios.get(
            'http://www.trataytrafico.gob.ec:8081/api/servicios/posicionamientoFiscalias'
            // 'http://52.43.113.195:8080/TEST/ministerio/api/test/posicionamientoUVC'
        );

        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ markersCasasAcogida: [] });

        let marcadores = [];
        // let marcadoresUVC = [];

        let i;

        for (i = 0; i < datos.length; i++) {
            let aux;
                aux = {
                    title: datos[i]['nombre'],
                    // description: 'Fiscalía de Personas Desaparecidas' + ' - ' + datos[i]['telefono']
                    description: 'Fiscalía Especializada 1' + ' - ' + datos[i]['telefono']
                };
                aux.location = { latitude: datos[i]['latitud'], longitude: datos[i]['longitud'] };
                aux.image = require('../assets/fiscalia.png');
                aux.anchor = { x: 0.125, y: 0.1 };
            marcadores.push(aux);
        }
        console.log(marcadores);
        this.setState({ markersUVC: marcadores });

  }

    async getCasasAcogida (location) {
        const { data } = await axios.get(
            // 'http://52.43.113.195:8080/TEST/ministerio/api/test/posicionamientoCasasAcogida'
            'http://www.trataytrafico.gob.ec:8081/api/servicios/posicionamientoCasasAcogida'
        );

        let datos = JSON.parse(JSON.stringify(data));
        this.setState({ markersCasasAcogida: [] });

        let marcadores = [];
        let i;

        for (i = 0; i < datos.length; i++) {
            let aux;
                aux = {
                    title: datos[i]['nombre'],
                    description: 'Casa Acogida'
                };
                aux.location = { latitude: datos[i]['latitud'], longitude: datos[i]['longitud'] };
                aux.image = require('../assets/casa.png');
                aux.anchor = { x: 0.125, y: 0.1 };
            marcadores.push(aux);
        }
        console.log(marcadores);
        this.setState({ markersCasasAcogida: marcadores });
  }


  getCurrentLocation = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      console.log('Permission to access location was denied');
      alert('Por favor, acepta los permisos para continuar');
    } else {
      console.log('Permission to access location granted');
      // alert('Permission to access location granted');
    }
      let location = await Location.getCurrentPositionAsync({
          accuracy: Location.Accuracy.BestForNavigation
      });

      console.log(location);
    this.getUPC(location);
    this.getUVC(location);
    // this.getCasasAcogida(location);

    this.setState({
      mapRegion: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.01922,
        longitudeDelta: 0.01621
      }
    });

    Location.watchPositionAsync(
      {
        accuracy: Location.Accuracy.BestForNavigation,
        timeInterval: 1000,
        distanceInterval: 10000
      },
      location =>
        this.setState({
          mapRegion: {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: 0.01922,
            longitudeDelta: 0.01621
          }
        })
    );
  };

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.headerView}>
              <Text style={styles.headerText}>DÓNDE DENUNCIAR</Text>
          </View>
        {/*<MenuButton navigation={this.props.navigation} />*/}
        <MapView
          style={styles.mapStyle}
          region={this.state.mapRegion}
          onRegionChangeComplete={this._handleMapRegionChange}
        >
          <Marker
            coordinate={{
              latitude: this.state.mapRegion.latitude,
              longitude: this.state.mapRegion.longitude
            }}
            title={'Posición actual'}
            description={'Usted está aquí'}
          />
            {/*//// UPC ////////////*/}

          {this.state.markersUPC.map(marker => (
            <Marker
              anchor={marker.anchor}
              key={marker.title}
              coordinate={marker.location}
              title={marker.title}
              description={marker.description}
            >
              <Image source={marker.image} style={{ width: 25, height: 25 }} />
            </Marker>
          ))}
          {/*//// UVC ////////////*/}
          {this.state.markersUVC.map(marker => (
            <Marker
              anchor={marker.anchor}
              key={marker.title}
              coordinate={marker.location}
              title={marker.title}
              description={marker.description}
            >
              <Image source={marker.image} style={{ width: 35, height: 35 }} />
            </Marker>
          ))}
          {/*//// casas acogidas ////////////*/}
          {this.state.markersCasasAcogida.map(marker => (
            <Marker
              anchor={marker.anchor}
              key={marker.title}
              coordinate={marker.location}
              title={marker.title}
              description={marker.description}
            >
              <Image source={marker.image} style={{ width: 20, height: 20 }} />
            </Marker>
          ))}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00ACBF',
    alignItems: 'center',
    justifyContent: 'center'
  },
    headerText :{
        color: '#fff',
        fontSize: 25
    },

    headerView: {
        backgroundColor: '#074d57',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 80,
        alignSelf: 'center',
        marginTop: Constants.statusBarHeight /2,
        marginBottom: Constants.statusBarHeight /2,
    },
  mapStyle: {
    width: Dimensions.get('window').width * 0.9,
    height: Dimensions.get('window').height * 0.7
  }
});
