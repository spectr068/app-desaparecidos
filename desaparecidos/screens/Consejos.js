import React from 'react';
import { Image, Dimensions, StyleSheet, Text, View, TouchableOpacity, ScrollView , SafeAreaView } from 'react-native';

import Constants from 'expo-constants';
import Modal from "react-native-modal";
import { FlatList } from 'react-native-gesture-handler';
import Carousel from "react-native-carousel-control";
import { FontAwesome } from '@expo/vector-icons';

export default class Consejos extends React.Component {
  state = {
      modalVisible: false,
      pageNumber: 0,

      modal1: {
          visible: false,
          pageNumber: 0
      },
      modal2: {
          visible: false,
          pageNumber: 0
      },
      modal3: {
          visible: false,
          pageNumber: 0
      },
      modal4: {
          visible: false,
          pageNumber: 0
      },

    activeSections: []
  };

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                style={styles.moduleView}
                onPress={() =>
                    // this.props.navigation.navigate(item.screen)
                {
                    if(item.key === 0){
                        this.setState({modal1:{visible: true, pageNumber: 0}})
                    }
                    if(item.key === 1){
                        this.setState({modal2:{visible: true, pageNumber: 0}})
                    }
                    if(item.key === 2){
                        this.setState({modal3:{visible: true, pageNumber: 0}})
                    }
                    if(item.key === 3){
                        this.setState({modal4:{visible: true, pageNumber: 0}})
                    }
                    // this.setState({modalVisible: true})
                    // this.setState({ pageNumber: item.key });
                }
                    // this.state.modalVisible =  true
                }
            >
                <Image source={item.image} style={styles.View_button} blurRadius={1} />
                {/*<Text style={styles.moduleName}>{item.name}</Text>*/}
            </TouchableOpacity>
        );
    };

  render() {
    return (
      <View style={styles.container}>

          <Modal isVisible={this.state.modal1.visible}>
              {/*<Modal >*/}
              <View style={styles.modalInfoCloseButtonContainer}>
                  <TouchableOpacity
                      onPress={() => this.setState({ modal1: {visible: false, pageNumber: 0 }})}
                      style={{ margin: 25 }}
                  >
                      <FontAwesome name="close" size={35} color="#fff" />
                  </TouchableOpacity>
              </View>
              <Carousel swipeThreshold={0.3} currentPage={ this.state.modal1.pageNumber } pageStyle={{backgroundColor: "white", borderRadius: 10, height: '90%'}}>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ HACER? </Text>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#69ab91'} ]}>

                          <Text style={styles.estiloTexto}>{que_hacer[0].texto}</Text>
                          <Image source={que_hacer[0].image} style={styles.LogosView}  />
                      </View>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#8aa698'} ]}>

                          <Text style={styles.estiloTexto}>{que_hacer[1].texto}</Text>
                          <Image source={que_hacer[1].image} style={styles.LogosView}/>

                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}>siguiente >>>>> </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ HACER?  </Text>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#84a868'} ]}>

                          <Text style={styles.estiloTexto}>{que_hacer[2].texto}</Text>
                          <Image source={que_hacer[2].image} style={styles.LogosView}  />

                      </View>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#8ea37d'} ]}>

                          <Text style={styles.estiloTexto}>{que_hacer[3].texto}</Text>
                          <Image source={que_hacer[3].image} style={styles.LogosView}  />

                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'} </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ HACER? </Text>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#afc76c'} ]}>

                      <Text style={styles.estiloTexto}>{que_hacer[4].texto}</Text>
                          <Image source={que_hacer[4].image} style={styles.LogosView}  />

                      </View>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#b5c284'} ]}>

                          <Text style={styles.estiloTexto}>{que_hacer[5].texto}</Text>
                          <Image source={que_hacer[5].image} style={styles.LogosView} />

                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'} </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ HACER?</Text>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#9ac4b9'} ]}>

                      <Text style={styles.estiloTexto}>{que_hacer[6].texto}</Text>
                          <Image source={que_hacer[6].image} style={styles.LogosView} />

                      </View>
                      <View style={[styles.estiloCuadro1, {backgroundColor: '#a4bcb5'} ]}>

                          <Text style={styles.estiloTexto}>{que_hacer[7].texto}</Text>
                          <Image source={que_hacer[7].image} style={styles.LogosView} />

                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás'} </Text>
                      </View>
                  </View>
              </Carousel>
          </Modal>

          <Modal isVisible={this.state.modal2.visible}>
              {/*<Modal >*/}
              <View style={styles.modalInfoCloseButtonContainer}>
                  <TouchableOpacity
                      onPress={() => this.setState({ modal2: {visible: false, pageNumber: 0 }})}
                      style={{ margin: 25 }}
                  >
                      <FontAwesome name="close" size={35} color="#fff" />
                  </TouchableOpacity>
              </View>
              <Carousel swipeThreshold={0.3} currentPage={ this.state.modal2.pageNumber } pageStyle={{backgroundColor: "white", borderRadius: 10, height: '90%'}}>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ NO DEBO HACER?</Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#f69030'} ]}>

                          <Text style={styles.estiloTexto}>{que_no_hacer[0].texto}</Text>
                          <Image source={que_no_hacer[0].image} style={styles.LogosView2} />

                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}>siguiente >>>>> </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ NO DEBO HACER? </Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#dfa55d'} ]}>
                          <Text style={styles.estiloTexto}>{que_no_hacer[1].texto}</Text>
                          <Image source={que_no_hacer[1].image} style={styles.LogosView2}  />
                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>
                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'} </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  ¿QUÉ NO DEBO HACER? </Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#febe7e'} ]}>

                          <Text style={styles.estiloTexto}>{que_no_hacer[2].texto}</Text>
                          <Image source={que_no_hacer[2].image} style={styles.LogosView2}  />
                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás '} </Text>
                      </View>
                  </View>

              </Carousel>
          </Modal>

            <Modal isVisible={this.state.modal3.visible}>
              {/*<Modal >*/}
              <View style={styles.modalInfoCloseButtonContainer}>
                  <TouchableOpacity
                      onPress={() => this.setState({ modal3: {visible: false, pageNumber: 0 }})}
                      style={{ margin: 25 }}
                  >
                      <FontAwesome name="close" size={35} color="#fff" />
                  </TouchableOpacity>
              </View>
              <Carousel swipeThreshold={0.3} currentPage={ this.state.modal3.pageNumber } pageStyle={{backgroundColor: "white", borderRadius: 10, height: '90%'}}>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel2}>¿QUÉ NECESITO PARA COLOCAR UNA DENUNCIA POR DESAPARICIÓN?</Text>

                      <View style={[styles.estiloCuadro2, {backgroundColor: '#af3b2e'} ]}>

                          <Text style={styles.estiloTexto}>{que_necesito[0].texto}</Text>
                          <Image source={que_necesito[0].image} style={styles.LogosView2} />
                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}>siguiente >>>>> </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel2}>¿QUÉ NECESITO PARA COLOCAR UNA DENUNCIA POR DESAPARICIÓN?</Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#b94c3d'} ]}>

                          <Text style={styles.estiloTexto}>{que_necesito[1].texto}</Text>
                          <Image source={que_necesito[1].image} style={styles.LogosView2} />
                      </View>

                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'} </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel2}>¿QUÉ NECESITO PARA COLOCAR UNA DENUNCIA POR DESAPARICIÓN?</Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#ae564c'} ]}>

                          <Text style={styles.estiloTexto}>{que_necesito[2].texto}</Text>
                          <Image source={que_necesito[2].image} style={styles.LogosView2}  />
                      </View>

                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'}</Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel2}>¿QUÉ NECESITO PARA COLOCAR UNA DENUNCIA POR DESAPARICIÓN?</Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#ae736e'} ]}>

                          <Text style={styles.estiloTexto}>{que_necesito[3].texto}</Text>
                          <Image source={que_necesito[3].image} style={styles.LogosView2}  />
                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás '} </Text>
                      </View>
                  </View>

              </Carousel>
          </Modal>

            <Modal isVisible={this.state.modal4.visible}>
              {/*<Modal >*/}
              <View style={styles.modalInfoCloseButtonContainer}>
                  <TouchableOpacity
                      onPress={() => this.setState({ modal4: {visible: false, pageNumber: 0 }})}
                      style={{ margin: 25 }}
                  >
                      <FontAwesome name="close" size={35} color="#fff" />
                  </TouchableOpacity>
              </View>
              <Carousel swipeThreshold={0.3} currentPage={ this.state.modal4.pageNumber } pageStyle={{backgroundColor: "white", borderRadius: 10, height: '90%'}}>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  PERSONA PERDIDA  </Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#166fad'} ]}>

                          <Text style={styles.estiloTexto4}>{informacion_adicional[0].texto}</Text>
                          <Image source={informacion_adicional[0].image} style={styles.LogosView2}  />
                      </View>

                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}>siguiente >>>>> </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  PERSONA EXTRAVIADA  </Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#367ba2'} ]}>

                          <Text style={styles.estiloTexto4}>{informacion_adicional[1].texto}</Text>
                          <Image source={informacion_adicional[1].image} style={styles.LogosView2}  />
                      </View>

                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'} </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  PERSONA DESAPARECIDA  </Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#367ba2'} ]}>

                          <Text style={styles.estiloTexto4}>{informacion_adicional[2].texto}</Text>

                          <Text style={[styles.estiloTexto4, {fontWeight: 'bold'} ]}>{'Involuntaria:'}</Text>
                          <Text style={styles.estiloTexto4}>{informacion_adicional[2].textoAdicional1}</Text>

                          <Text style={[styles.estiloTexto4, {fontWeight: 'bold',}]}>{'Voluntaria:'}</Text>
                          <Text style={styles.estiloTexto4}>{informacion_adicional[2].textoAdicional2}</Text>

                          {/*<Image source={informacion_adicional[2].image} style={styles.LogosView2}  />*/}
                      </View>

                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás || siguiente >>>>>'} </Text>
                      </View>
                  </View>
                  <View style={styles.carouselView}>
                      <Text style={styles.titleCarousel}>  DESAPARICIÓN FORZADA  </Text>
                      <View style={[styles.estiloCuadro2, {backgroundColor: '#5d87a0'} ]}>

                          <Text style={styles.estiloTexto4}>{informacion_adicional[3].texto}</Text>
                          <Image source={informacion_adicional[2].image} style={styles.LogosView2} />
                      </View>
                      <View style={[styles.estiloCuadro1Fin ]}>

                          <Text style={styles.estiloTextoFin}> {'<<<<< atrás '} </Text>
                      </View>
                  </View>

              </Carousel>
          </Modal>




          {/*<Modal isVisible={this.state.modalVisible}>*/}
              {/*/!*<Modal >*!/*/}
              {/*<View style={styles.modalInfoCloseButtonContainer}>*/}
                  {/*<TouchableOpacity*/}
                      {/*onPress={() => this.setState({ modalVisible: false })}*/}
                      {/*style={{ margin: 25 }}*/}
                  {/*>*/}
                      {/*<FontAwesome name="close" size={35} color="#fff" />*/}
                  {/*</TouchableOpacity>*/}
              {/*</View>*/}
              {/*<Carousel swipeThreshold={0.3} currentPage={ this.state.pageNumber } pageStyle={{backgroundColor: "white", borderRadius: 10, height: '90%'}}>*/}
                  {/*<View style={styles.carouselView}>*/}
                      {/*<Text style={styles.titleCarousel}>  ¿QUÉ HACER? - 1  </Text>*/}

                        {/*<View>*/}
                            {/*<SafeAreaView style={styles.safeAreaContainer}>*/}
                                {/*<ScrollView style={styles.scrollView} persistentScrollbar={true}>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[0].texto}</Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[1].texto}</Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[2].texto}</Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[3].texto}</Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[4].texto}</Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[5].texto}</Text>*/}
                                    {/*</View>*/}
                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[6].texto}</Text>*/}
                                    {/*</View>*/}

                                    {/*<View style={[styles.estiloCuadro1, {backgroundColor: '#f0df28'} ]}>*/}
                                        {/*<Text style={styles.estiloTexto}>{que_hacer[7].texto}</Text>*/}
                                    {/*</View>*/}

                                {/*</ScrollView>*/}
                            {/*</SafeAreaView>*/}
                        {/*</View>*/}

                  {/*</View>*/}
                  {/*<View style={styles.carouselView}>*/}
                      {/*<Text style={styles.titleCarousel}>  ¿QUÉ NO DEBO HACER?  </Text>*/}


                  {/*</View>*/}
                  {/*<View style={styles.carouselView}>*/}
                      {/*<Text style={styles.titleCarousel}>  ¿QUÉ NECESITO?  </Text>*/}

                  {/*</View>*/}
                  {/*<View style={styles.carouselView}>*/}
                      {/*<Text style={styles.titleCarousel}>  INFORMACIÓN ADICIONAL  </Text>*/}


                  {/*</View>*/}

              {/*</Carousel>*/}

          {/*</Modal>*/}
          {/*<View style={styles.headerView}>*/}
              {/*<Image style={styles.headerView_logo} source={require('../assets/consejos/titulo.png')} />*/}
          {/*</View>*/}


          <View style={styles.headerView}>
              <Text style={styles.headerText}>EN UNA DESAPARICIÓN</Text>
          </View>


          {/*<View style={{ flex: 1 }}>*/}
              {/*<Image source={require('../assets/consejos/titulo.png')} style={styles.headerView} blurRadius={1} />*/}
          {/*</View>*/}


          <FlatList
              data={modules}
              style={styles.modulesContainer}
              contentContainerStyle={{}}
              renderItem={this.renderItem}
              numColumns={2}
          />




        {/*/!*<View style={styles.headerView}>*!/*/}
          {/*<Image style={styles.headerView_logo} source={require('../assets/consejos/imagen_presentacion.png')} />*/}
        {/*/!*</View>*!/*/}
          {/*<TouchableOpacity*/}
              {/*onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}*/}
              {/*style={{ margin: 25 }}*/}
          {/*>*/}
              {/*<Image source={require('../assets/consejos/imagen_presentacion.png')} style={styles.ImagesView} blurRadius={1} />*/}

            {/*<Text>I am the modal content!</Text>*/}

          {/*</TouchableOpacity>*/}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00ACBF',
    alignItems: 'center',
    justifyContent: 'center'
  },
    headerText :{
        color: '#fff',
        fontSize: 25
    },

    headerView: {
        backgroundColor: '#074d57',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 80,
        alignSelf: 'center',
        marginTop: Constants.statusBarHeight /2,
        marginBottom: Constants.statusBarHeight /2,
    },
    headerView_logo: {
        width: '100%',
        height: '100%'
    },
    carousel:{
      flex: 6,
        width: '100%',
        height: '100%'
    },
    carouselView : {
      // flex:10,
        backgroundColor: '#00ACBF',
        width: '100%',
        height: '80%',
        alignItems: 'center',
        justifyContent: 'center',

    },
    titleCarousel: {
      flex:1,
        color: '#fff',
        backgroundColor: '#074d57',
        fontSize: 20,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center',
        // width: '100%'
        // width: '100%',

    },
    titleCarousel2: {
      flex:3,
        color: '#fff',
        backgroundColor: '#074d57',
        fontSize: 20,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'center',
        // width: '100%'
        // width: '100%',

    },
    modalInfoCloseButtonContainer: {
        // flex: 1,
        // height: 20,
        alignItems: 'flex-end'
    },
  View_button: {
      width: '100%',
      height: undefined,
      aspectRatio: 1,
  },
    View_titulo: {
    width: (Dimensions.get('window').width ),
      height: undefined,
      aspectRatio: 1,
  },
    modulesContainer: {
        flex: 1,
        width: '100%',
        marginVertical: 20,

    },
    moduleView: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        // marginHorizontal: 5,
        // marginVertical: 5,
        // height: (Dimensions.get('window').height - 80 - 60 - 10 * 2) / 2 // approximate a square

    },
    ImagesView: {
        position: 'absolute',
        height: (Dimensions.get('window').height - 80 - 60 - 10 * 2) / 2, // approximate a square

        width: (Dimensions.get('window').width )/2 , // approximate a square
        borderRadius: 5
    },
    LogosView: {
        // position: 'absolute',
        height: undefined, // approximate a square

        width:  '25%' , // approximate a square
        aspectRatio: 1,
        alignSelf: 'center'
    },
    LogosView2: {
        // position: 'absolute',
        height: undefined, // approximate a square

        width:  '50%' , // approximate a square
        aspectRatio: 1,
        alignSelf: 'center'
    },
    moduleName: {
        fontSize: 14,
        textAlign: 'center',
        color: '#fff',
        alignSelf: 'center'
    },

  contentContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#0E406B'
  },
  accordionContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: '#0E406B'
  },
  accordionItem_header: {
    backgroundColor: '#0E406B',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    height: 70
  },
  accordionItem_headerText: {
    fontSize: 20,
    color: '#fff'
  },
  accordionItem_content: {
    paddingVertical: 10,
    backgroundColor: '#03C8D5'
  },

    safeAreaContainer:{
        flex: 1,
    },

    scrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 5,

    },

    estiloTexto: {
        color: '#fff',
        // backgroundColor: '#ff0000',
        // borderColor: '#fff',
        // borderColor: '#074d57',
        fontSize: (Dimensions.get('window').width) / 23,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'justify'

        // borderRadius: 5,
        // margin: 8,
        // padding: 3,
        // borderWidth: 2
    },
    estiloTexto4: {
        color: '#fff',
        // backgroundColor: '#ff0000',
        // borderColor: '#fff',
        // borderColor: '#074d57',
        fontSize: (Dimensions.get('window').width) / 33,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'justify'

        // borderRadius: 5,
        // margin: 8,
        // padding: 3,
        // borderWidth: 2
    },
    estiloCuadro1:{
      flex:5,
        backgroundColor: '#ff0000',
        borderColor: '#fff',
        borderRadius: 5,
        margin: 8,
        padding: 3,
        borderWidth: 2,
        justifyContent: 'center',
    },
    estiloCuadro2:{
      flex:10,
        backgroundColor: '#ff0000',
        borderColor: '#fff',
        borderRadius: 5,
        margin: 8,
        padding: 3,
        borderWidth: 2,
        justifyContent: 'center',
    },
    estiloCuadro1Fin:{
      flex: 1
    },
    estiloTextoFin:{
        color: '#fff',
        fontSize: (Dimensions.get('window').width) / 18,
        alignItems: 'center',
        justifyContent: 'center',
    }
});



const modules = [
    {
        key: 0,
        name: 'que hacer',
        screen: 'Info',
        image: require('../assets/consejos/imagen1.png')
    },
    {
        key: 1,
        name: 'que no debo hacer',
        screen: 'Places',
        image: require('../assets/consejos/imagen2.png')
    },
    {
        key: 2,
        name: 'que necesito',
        screen: 'MissingPeople',
        image: require('../assets/consejos/imagen3.png')
    },
    {
        key: 3,
        name: 'informacion adicional',
        screen: 'FindingFamily',
        image: require('../assets/consejos/imagen4.png')
    },

    // ,
    // {
    //   key: 8,
    //   name: 'Registro de personas desconocidas',
    //   screen: 'UnknownPeopleRegistration',
    //     image: require('../assets/home/fondoBoton.png')

    // }
];


const que_hacer = [
    {
        texto: 'Acudir a la UPC más cercana o llamar al 911 para informar lo sucedido',
        image: require('../assets/consejos/logos-01.png')
    },
    {
        texto: 'Acércate a la FISCALÍA, DINASED o DINAPEN para la denuncia',
        image: require('../assets/consejos/logos-02.png')

    },
    {
        texto: 'Generar una lista de contactos de personas cercanas a la persona desaparecida',
        image: require('../assets/consejos/logos-03.png')

    },
    {
        texto: 'Recordar las actividades planificadas de la persona desaparecida',
        image: require('../assets/consejos/logos-04.png')

    },
    {
        texto: 'Proveer de información acerca de los hábitos de la persona desaparecida',
        image: require('../assets/consejos/logos-05.png')

    },
    {
        texto: 'Colaborar con las diligencias, entrevistas, llamadas, consultas que realice el investigador',
        image: require('../assets/consejos/logos-06.png')

    },
    {
        texto: 'Difundir únicamente el afiche de información generado por la autoridad competente',
        image: require('../assets/consejos/logos-07.png')

    },
    {
        texto: 'Cuando la persona desaparecida retorne al hogar o haya sido localizada, informe a las autoridades',
        image: require('../assets/consejos/logos-08.png')

    },

];

const que_no_hacer = [
    {
        texto: 'Emprender la búsqueda por usted mismo',
        image: require('../assets/consejos/logos-09.png')

    },
    {
        texto: 'No divulgue datos personales como número de telefónico y dirección del domicilio',
        image: require('../assets/consejos/logos-10.png')

    },
    {
        texto: 'No negocie o entregue dinero a pesar de que reciba amenazas o presiones',
        image: require('../assets/consejos/logos-11.png')

    },

];

const que_necesito = [
    {
        texto: 'Fotografía más reciente en forma digital o física',
        image: require('../assets/consejos/logos-12.png')

    },
    {
        texto: 'Datos de identidad de la persona desaparecida',
        image: require('../assets/consejos/logos-13.png')

    },
    {
        texto: 'Descripción detallada de los hechos con las características físicas y rasgos diferenciales, si recuerda el detalle de la ropa que llevaba la persona al momento de desaparecer',
        image: require('../assets/consejos/logos-14.png')

    },
  {
        texto: 'Otra información relevante de las circunstancias de la desaparición',
      image: require('../assets/consejos/logos-15.png')

  },

];

const informacion_adicional = [
    {
        texto: 'Se considera persona perdida a las niñas, niños, adolescentes, cuya ausencia voluntaria o involuntaria del hogar, establecimiento educativo u otro lugar en el que se supone deben permanecer, sin el consentimiento o conocimiento de sus progenitores o responsables de su cuidado',
        image: require('../assets/consejos/logos-16.png')

    },
    {
        texto: 'Es la ausencia temporal de una persona debido a accidentes, desastres o catástrofes naturales o antrópicos, pudiendo también ser causada por discapacidad o enfermedad, que le imposibilita tener la aptitud, los medios o recursos necesarios para retornar a su entorno habitual. En estos casos, la ausencia de la persona no es causada por un tercero',
        image: require('../assets/consejos/logos-17.png')

    },
    {
        texto: 'Es la ausencia de una persona de su núcleo familiar o entorno, sin que se conozca el paradero o las causas que la motivaron. La desaparición puede ser:',
        image: require('../assets/consejos/logos-18.png'),
        textoAdicional1: 'Es la ausencia ligada a la acción de otra persona sin que medie decisión o intención propia. Según el Código Integral Penal, la persona que prive de la libertad, retenga, arrebate, desaparezca, traslade a lugar distinto a una o más personas, en contra de su voluntad y niegue información de su paradero o destino, será sancionada con pena privativa de libertad de 7 a 10 años',
        textoAdicional2: 'Es la ausencia de una persona de su núcleo familiar o entorno, motivada por su decisión e intención propia'

    },
    {
        texto: '' +
            'Se produce cuando un agente del Estado o quien actúe con su consentimiento, por cualquier medio, someta a privación de libertad a una persona, seguida de la falta de información o de la negativa a reconocer dicha privación de libertad o de informar sobre el paradero o destino de una persona, con lo cual se impida el ejercicio de garantías constitucionales o legales.\n\n Este delito es sancionado con pena privativa de libertad de 22 a 26 años',
        image: require('../assets/consejos/logos-18.png')

    },

];
// const informacion_adicional = [
//     {
//         texto: 'Se considera como persona desaparecida a toda persona que se encuentre en paradero desconocido para sus familiares, amigos y otras personas cercanas.',
//         image: require('../assets/consejos/logos-16.png')
//
//     },
//     {
//         texto: 'Se considera como persona extraviada a aquella que luego de salir de su domicilio o de algún otro lugar, y por causas ajenas a su voluntad no puede regresar.',
//         image: require('../assets/consejos/logos-17.png')
//
//     },
//     {
//         texto: 'Según la definición del Código de la Niñez y Adolescencia; se considera persona perdida a las niñas, niños, adolescentes, cuya ausencia voluntaria o involuntaria del hogar, establecimiento educativo u otro lugar en el que se supone deben permanecer, sin el consentimiento o conocimiento de sus progenitores o responsables de su cuidado.',
//         image: require('../assets/consejos/logos-18.png')
//
//     },
//
// ];
