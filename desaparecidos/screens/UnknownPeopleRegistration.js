// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';
// import MenuButton from '../components/MenuButton';
//
// export default class UnknownPeopleRegistration extends React.Component {
//   render () {
//     return (
//       <View style={styles.container}>
//         <MenuButton  navigation={this.props.navigation} />
//         <Text>REGISTRO DE PERSONAS DESCONOCIDAS</Text>
//       </View>
//     );
//   }
// }
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });


import React from 'react';
import {StyleSheet, SafeAreaView, Text, View, Dimensions, Image} from 'react-native';
import MenuButton from '../components/MenuButton';

import FormButton from '../components/FormButton'

import { Input } from 'react-native-elements'
import {Ionicons} from "@expo/vector-icons";

import axios from 'axios';
import * as Permissions from "expo-permissions";
import {NavigationContainer} from "@react-navigation/native";
import Constants from "./Home";

export default class UnknownPeopleRegistration extends React.Component {

    state = {
        user: '',
        password: ''
    }

    constructor(props){
        super(props);
        this.state.user  = '';
        // this.state.user  = '0202095899';
        this.state.password  = '';
        // this.state.password  = '123456';


    }

    handleUserChange = user => {
        this.setState({ user })
    }

    handlePasswordChange = password => {
        this.setState({ password })
    }

    onLogin = async () => {
        const camera = await Permissions.askAsync(Permissions.CAMERA);
        const hasCameraPermission = camera.status === 'granted';
        this.setState({ hasCameraPermission });


        const { user, password } = this.state;
        console.log("ingreso a forms")
        try {
            if (user.length > 0 && password.length > 0) {
                // this.props.navigation.navigate('App')
                let bodyForm = {
                    "aplicacion": "PAGDESAP",
                    usuario: user,
                    clave: password
                };
                let urlLogin = 'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/login';

                let requestOptions = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                };

                // const { data } = await axios.post( urlLogin,bodyForm);
                //
                //
                // let datos = JSON.parse(JSON.stringify(data));
                //
                // if ()
                // console.log(datos);
                return axios.post(urlLogin, bodyForm)
                    .then(response => {
                        console.log('ok ')
                        // this.props.token = response.data.token;
                        // console.log(response.data.token);
                        this.setState({ user: '' });
                        this.setState({ password: '' });
                        this.props.navigation.navigate("RegisterForm", {
                            token: response.data.token,
                            usuario: user
                            // otherParam: 'anything you want here',
                        });
                        // return;
                    })
                    .catch(err => {
                        console.log('error');
                        alert('error de credenciales');
                        this.setState({ user: '' });
                        this.setState({ password: '' });
                        return ;
                    });

            }
        } catch (error) {
            alert(error)
        }
    };
    goToSignup = () => this.props.navigation.navigate('Signup')

    render () {
        const { user, password } = this.state
        return (
            <View style={styles.containerGlobal}>
                <MenuButton  navigation={this.props.navigation} />
                {/*<View style={styles.headerView}>*/}
                {/*    <Image style={styles.headerView_logo1} source={require('../assets/logo2.png')} />*/}
                {/*    <Image style={styles.headerView_logo2} source={require('../assets/main-logo.png')} />*/}
                {/*</View>*/}

                <View style={styles.container}>
                    <Text style={styles.titulo}>FORMULARIO DE ACCESO AL REGISTRO NACIONAL DE PERSONAS SIN IDENTIDAD</Text>

                    <Input
                        leftIcon={<Ionicons name="md-people" size={28} color="#2C384A" />}
                        leftIconContainerStyle={styles.iconStyle}
                        placeholderTextColor="grey"
                        name="usuario"
                        value={user}
                        placeholder="Ingrese usuario"
                        style={styles.input}
                        onChangeText={this.handleUserChange}
                    />
                    <Input
                        leftIcon={<Ionicons name="ios-lock" size={28} color="#2C384A" />}
                        leftIconContainerStyle={styles.iconStyle}
                        placeholderTextColor="grey"
                        secureTextEntry
                        name="password"
                        value={password}
                        placeholder="Ingrese password"
                        style={styles.input}
                        onChangeText={this.handlePasswordChange}
                    />

                    <View style={styles.buttonContainer}>
                        <FormButton
                            buttonType="outline"
                            onPress={this.onLogin}
                            title="INGRESO"
                            buttonColor="#039BE5"
                        />
                    </View>
                    <Text style={styles.textoRecuperacion}>¿Desea recuperar su contraseña?</Text>

                    <Text style={styles.textoFinal}>Soporte téctnico las 24 horas, marcando al número telefónico (02) 3834444 opción 2, ó al correo electrónico</Text>
                    <Text style={[styles.textoFinal, {color: '#0080ec'}]}>soporte.des@ministeriodegobierno.gob.ec</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerGlobal: {
        flex:1 ,
        backgroundColor: '#00ACBF',
        justifyContent: 'center',
    },
    container: {
        // flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: (Dimensions.get('window').height) *0.75,
    },
    headerView: {
        width: '75%',
        height: 80,
        // alignSelf: 'center',
        marginTop: Constants.statusBarHeight,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerView_logo1: {
        flex:1,
        width: '30%',
        height: undefined,
        aspectRatio: 2,
        justifyContent: 'center',
        alignItems: 'center',

    },
    headerView_logo2: {
        flex:2,
        width: '70%',
        height: '100%'
    },


    buttonContainer: {
        margin: 25
    },
    iconStyle: {
        marginRight: 10
    },

    titulo:{
        textAlign: 'center',
        fontSize: (Dimensions.get('window').width) / 16,
        fontWeight: 'bold',
        marginBottom: 25,
        // color: '#074d57',
        color: '#0080ec',

    },
    textoFinal: {
        textAlign: 'justify',
        marginLeft: 25,
        marginRight: 25,
        marginTop: 10,
    },
    textoRecuperacion: {
        color: '#0080ec',


    }

});
