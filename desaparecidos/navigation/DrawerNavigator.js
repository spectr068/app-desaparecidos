import React from 'react';
import { Platform, Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Home from '../screens/Home';
import Info from '../screens/Info';
import Places from '../screens/Places';
import MissingPeople from '../screens/MissingPeople';
import FindingFamily from '../screens/FindingFamily';
import SolvedCases from '../screens/SolvedCases';
import Campaigns from '../screens/Campaigns';
import EmilyAlert from '../screens/EmilyAlert';
import UserRegistration from '../screens/UserRegistration';
import UnknownPeopleRegistration from '../screens/UnknownPeopleRegistration';

import Camara from '../screens/utilidades/camara';
import RegisterForm from '../screens/utilidades/RegisterForm';
import MenuDrawer from '../components/MenuDrawer';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0,
  contentComponent: ({ navigation }) => <MenuDrawer navigation={navigation} />
};

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        drawerLabel: 'Inicio'
      }
    },
    Info: {
      screen: Info,
      navigationOptions: {
        drawerLabel: 'Información'
      }
    },
    Places: {
      screen: Places,
      navigationOptions: {
        drawerLabel: 'Lugares donde denunciar'
      }
    },
    MissingPeople: {
      screen: MissingPeople,
      navigationOptions: {
        drawerLabel: 'Personas desaparecidas'
      }
    },
    FindingFamily: {
      screen: FindingFamily,
      navigationOptions: {
        drawerLabel: 'Busco a mis familiares'
      }
    },
    SolvedCases: {
      screen: SolvedCases,
      navigationOptions: {
        drawerLabel: 'Casos resueltos'
      }
    },
    Campaigns: {
      screen: Campaigns,
      navigationOptions: {
        drawerLabel: 'Campañas'
      }
    },
    EmilyAlert: {
      screen: EmilyAlert,
      navigationOptions: {
        drawerLabel: 'Alerta Emilia'
      }
    },
    UserRegistration: {
      screen: UserRegistration,
      navigationOptions: {
        drawerLabel: 'Registro de usuarios'
      }
    },
    UnknownPeopleRegistration: {
      screen: UnknownPeopleRegistration,
      navigationOptions: {
        drawerLabel: 'Registro de personas desconocidas'
      }
    },
    TEST: {
      screen: Camara,
      navigationOptions: {
        drawerLabel: 'TEST WINDOW'
      }
    },
    RegisterForm: {
      screen: RegisterForm
    }
  },
  DrawerConfig
);

export default createAppContainer(DrawerNavigator);
