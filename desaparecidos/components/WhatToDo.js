import React, { Fragment } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class WhatToDo extends React.Component {
  render() {
    return (
      <Fragment>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info8.png')} />
          <Text style={styles.presentationText}>
            Fotografía reciente en forma digital o física.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info10.png')} />
          <Text style={styles.presentationText}>
            Datos de identidad de la persona desaparecida.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info9.png')} />
          <Text style={styles.presentationText}>
            Descripción detallada de los hechos, con todas las características físicas y rasgos
            diferenciales; detalle de la ropa que llevaba en el momento de desaparecer.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info11.png')} />
          <Text style={styles.presentationText}>
            Otra información relevante de las circunstancias de la desaparición.
          </Text>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  instructionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10
  },
  instructionImage: {
    maxHeight: 60,
    maxWidth: 80,
    flex: 3,
    marginRight: 10
  },
  presentationText: {
    color: '#fff',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify',
    flex: 7
  }
});
