import React, { Fragment } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class WhatToDoEmilyAlert extends React.Component {
  render() {
    return (
      <Fragment>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/alertagraph1.png')} />
          <Text style={styles.presentationText}>
            Si ves uno de estos y tienes información sigue los siguientes pasos:
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/alertagraph2.png')} />
          <Text style={styles.presentationText}>
            Cuando recibes información de un menor de edad desaparecido presta más atención a los
            anuncios de los diferentes medios como: TV, afiches, mensajes de texto, radio y redes
            sociales.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/alertagraph3.png')} />
          <Text style={styles.presentationText}>
            Mantente atento a tu alrededor para compartir y notificar si cuentas con información que
            ayude a la ubicación del menor de edad desaparecido comunicarse a: 1800– DELITO (335486)
            o al 911.
          </Text>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  instructionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10
  },
  instructionImage: {
    maxHeight: 60,
    maxWidth: 80,
    flex: 3,
    marginRight: 10
  },
  presentationText: {
    color: '#053862',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify',
    flex: 7
  }
});
