import React, { Fragment } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default class WhatNotToDo extends React.Component {
  render() {
    return (
      <Fragment>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info12.png')} />
          <Text style={styles.presentationText}>
            No debe emprender la búsqueda por usted mismo. Tampoco negocie o entregue dinero en caso
            que reciba amenazas o presiones.
          </Text>
        </View>
        <View style={styles.instructionContainer}>
          <Image style={styles.instructionImage} source={require('../assets/info13.png')} />
          <Text style={styles.presentationText}>
            El uso de las redes sociales es importante en estos casos, sin embargo es recomendable
            no divulgar datos personales de familiares como: número de teléfono y dirección de
            domicilio.
          </Text>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  instructionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10
  },
  instructionImage: {
    height: 60,
    width: 80,
    flex: 2,
    marginRight: 10
  },
  presentationText: {
    color: '#fff',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify',
    flex: 8
  }
});
