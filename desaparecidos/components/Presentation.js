import React, { Fragment } from 'react';
import { StyleSheet, Text } from 'react-native';

export default class Presentation extends React.Component {
  render() {
    return (
      <Fragment>
        <Text style={styles.presentationText}>
          El portal web: www.desaparecidosecuador.gob.ec busca sensibilizar a la comunidad sobre la
          problemática de las personas desaparecidas.
        </Text>
        <Text style={styles.presentationText}>
          Además, es un servicio de apoyo al trabajo que sobre este tema desarrolla la Policía
          Nacional, la Fiscalía General y el Servicio Nacional de Medicina Legal y Ciencias
          Forenses. De esa forma, propicia la participación ciudadana.
        </Text>
        <Text style={styles.presentationText}>
          Esta página web es administrada por el Ministerio de Gobierno y fue creada con el
          objetivo de mejorar la difusión de las imágenes de las personas desaparecidas y así,
          evitar la desinformación y especulación.
        </Text>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  presentationText: {
    color: '#fff',
    fontSize: 16,
    margin: 5,
    textAlign: 'justify'
  }
});
