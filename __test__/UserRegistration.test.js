import React from 'react';
import UserRegistration from '../screens/UserRegistration';

import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<UserRegistration />).toJSON();
  expect(tree).toMatchSnapshot();
});
