import React from 'react';
import {Dimensions, Text, View, TouchableOpacity } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import * as FileSystem from 'expo-file-system';
// import RNFS from 'react-native-filesystem';
// import * as RNFS from 'react-native-fs';
import * as MediaLibrary from 'expo-media-library';

export default class Camara extends React.Component {
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
    };

    onPictureSaved = async photo => {
        this.setState({photo : photo.uri});


        console.log(photo.uri);

        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === "granted") {

            const asset = await MediaLibrary.createAssetAsync(photo.uri);
            console.log(asset);

            await MediaLibrary.createAlbumAsync("Ministerio/fotos", asset, false)





        }
        // alert(this.state.photo);
        // var destPath = RNFS.DocumentDirectoryPath + '/' + 'name';


        // console.log(this.state);

        // MediaLibrary.createAlbumAsync('Expo', asset)
        //     .then(() => {
        //         console.log('Album created!');
        //     })
        //     .catch(error => {
        //         console.log('err', error);
        //     });

        //
        // // const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
        //     // const { status2 } = await Permissions.askAsync(Permissions.);
        //     // if (status2 === "granted") {
        //
        //
        //         await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + 'photos2/')
        //
        //     // }
        // }

        // alert(FileSystem.documentDirectory);
        // alert(this.state.photoId);
        // envio de foto, almacenamiento y envio a la nube
        // await FileSystem.makeDirectoryAsync( 'file:///photos/')
        // await FileSystem.makeDirectoryAsync( FileSystem.documentDirectory+'photos/')



    };

    takePictureAndCreateAlbum = async () => {
        // alert('foto capturada');
        if (this.camera) {

            const options = { quality: 1, base64: true, fixOrientation: true,
                exif: true, skipProcessing: true, aspect: [4, 3]};

            this.camera.takePictureAsync(options).then((data) => {

                this.onPictureSaved(data);
            });
        }


        // console.log('tpaca');
        // const { uri } = await this.camera.takePictureAsync();
        // console.log('uri', uri);
        // const asset = await MediaLibrary.createAssetAsync(uri);
        // console.log('asset', asset);
        // MediaLibrary.createAlbumAsync('Expo', asset)
        //     .then(() => {
        //         Alert.alert('Album created!')
        //     })
        //     .catch(error => {
        //         Alert.alert('An Error Occurred!')
        //     });
    };


    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Camera
                        style={{ flex: 1 }}
                        type={this.state.type}
                        ref={ref => {
                            this.camera = ref;
                        }}
                    >

                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                                height: (Dimensions.get('window').height - 80 -60 -10*5)/ 5, // approximate a square
                                width: (Dimensions.get('window').width -10*2)/ 2, // approximate a square
                            }}>
                            <TouchableOpacity
                                style={{
                                    flex: 1,
                                    alignSelf: 'flex-end',
                                    // alignItems: 'center',

                                }}
                                onPress={() => {
                                    this.takePictureAndCreateAlbum();
                                    // alert('ytygjgjh');

                                    // console.warn("Warning message");
                                    // this.takePicture.bind(this);
                                    // this.setState({
                                    //     type:
                                    //         this.state.type === Camera.Constants.Type.back
                                    //             ? Camera.Constants.Type.front
                                    //             : Camera.Constants.Type.back,
                                    // });
                                }}>
                                <Text style={{
                                    fontSize: 18,
                                    marginBottom: 10,
                                    color: 'white',
                                    textAlign:'center',
                                    backgroundColor:'#00a0c0'
                                }}> CAPTURAR </Text>
                            </TouchableOpacity>
                        </View>

                    </Camera>
                    {/*<View>*/}
                    {/*<TouchableOpacity*/}
                    {/*onPress={this.takePicture.bind(this)} >*/}
                    {/*<Text>Take photo</Text>*/}
                    {/*</TouchableOpacity>*/}
                    {/*</View>*/}
                </View>
            );
        }
    }
}