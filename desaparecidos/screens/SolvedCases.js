import React from 'react';
import { Image, Dimensions, StyleSheet, FlatList, View } from 'react-native';
import MenuButton from '../components/MenuButton';
import Constants from 'expo-constants';
import axios from 'axios';
import SolvedCaseCard from '../components/SolvedCaseCard';

export default class SolvedCases extends React.Component {
  state = {
    solvedCases: [],
    next: ''
  };

  componentDidMount() {
    this.getSolvedCases(1);
  }

  getSolvedCases = async page => {
    try {
      const response = await axios.get(
        `http://181.113.21.13:28080/registroservicios-war/webresources/api/busqueda/resueltos?pagina=${page}`
      );
      this.setState({ solvedCases: response.data.registros, next: response.data.siguiente });
    } catch (error) {
      console.error(error);
    }
  };
  showMore = async () => {
    try {
      const response = await axios.get(this.state.next);
      this.setState({
        solvedCases: this.state.solvedCases.concat(response.data.registros),
        next: response.data.siguiente
      });
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <MenuButton navigation={this.props.navigation} />
        <View style={styles.headerView}>
          <Image style={styles.headerView_logo} source={require('../assets/main-logo.png')} />
        </View>
        <FlatList
          style={styles.peopleContainer}
          data={this.state.solvedCases}
          keyExtractor={item => item.idDesaparecido}
          numColumns="2"
          renderItem={({ item }) => (
            <SolvedCaseCard key={item.idDesaparecido} nombres={item.nombres} />
          )}
          onEndReached={() => this.showMore()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerView: {
    width: '50%',
    height: 80,
    alignSelf: 'center',
    marginTop: Constants.statusBarHeight
  },
  headerView_logo: {
    width: '100%',
    height: '100%'
  },
  peopleContainer: {
    flex: 1,
    width: '100%'
  },
  showMoreButton: {
    height: 60,
    width: Dimensions.get('window').width,
    backgroundColor: '#053862',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
