import React from 'react';
// import { StyleSheet } from 'react-native';
import {StyleSheet, Text, TouchableOpacity, Button, View} from 'react-native';

import { Ionicons } from '@expo/vector-icons';


// import Menu, { MenuItem, MenuDivider, Position } from "react-native-enhanced-popup-menu";
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Constants from "expo-constants";

// import {
//     Menu,
//     MenuOptions,
//     MenuOption,
//     MenuTrigger,
//     MenuProvider
// } from 'react-native-popup-menu';


// let textRef = React.createRef();
// let menuRef = null;
//
// const setMenuRef = ref => menuRef = ref;
// const hideMenu = () => menuRef.hide();
// const showMenu = () => menuRef.show(textRef.current);
// const onPress = () => showMenu();

export default class MenuButton extends React.Component {

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
        {/*<Text onPress={this.showMenu}>menú</Text>*/}
        console.log('press');
    };

    render () {
    return (

        <View style={styles.menuIcon}>
            <Menu
                ref={this.setMenuRef}
                button={

                    <Ionicons name="md-menu" size={32}   onPress={this.showMenu}/>
                    // <Button
                    //     normal
                    //     iconLeft
                    //     fontWeight='bold'
                    //     backgroundColor='#074d57'
                    //
                    //     icon={{name: 'menu', type: 'font-awesome'}}
                    //     title='MENÚ'
                    //     onPress={this.showMenu}
                    // />


                }

            >
                <MenuItem onPress={()=>
                    {
                        // this.hideMenu;
                        this.props.navigation.navigate("Home");
                    }
                }>Inicio</MenuItem>
                <MenuItem onPress={()=>
                    {
                        // this.hideMenu;
                        this.props.navigation.navigate("UnknownPeopleRegistration");
                    }
                }
                >Ingreso</MenuItem>
                <MenuItem onPress={ () =>
                    {
                        this.props.navigation.navigate("UserRegistration");
                }
                    // this.hideMenu
                }>Crear Usuario</MenuItem>

                {/*<MenuItem onPress={this.hideMenu} disabled>*/}
                    {/*Menu item 3*/}
                {/*</MenuItem>*/}
                {/*<MenuDivider />*/}
                {/*<MenuItem onPress={this.hideMenu}>Menu item 4</MenuItem>*/}
            </Menu>
        </View>

    )
  }
}





// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         paddingTop: 50,
//         backgroundColor: '#ecf0f1',
//     },
// });


const styles = StyleSheet.create({
  menuIcon: {
    zIndex: 9,
    position: 'absolute',
      marginTop: Constants.statusBarHeight,

      top: 10,
    right: 10,
      justifyContent: 'center'
  },
    test:{
      backgroundColor: '#ff0000',
        position: 'absolute',


    }
});
