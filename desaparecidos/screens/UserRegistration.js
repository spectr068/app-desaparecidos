import React from 'react';
import {
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView
} from 'react-native';
import MenuButton from '../components/MenuButton';
import { FontAwesome } from '@expo/vector-icons';
import Constants from 'expo-constants';
import FormButton from "../components/FormButton";
import Modal from "react-native-modal";

// import Textarea from 'react-native-textarea';

import CheckBox from 'react-native-check-box'
import axios from "axios";
import {Dialog} from "react-native-simple-dialogs";

export default class UserRegistration extends React.Component {
  state = {
    cedula: '',
    nombre: '',
    direccion: '',
    telefonoOficina: '',
    telefonoDomicilio: '',
    telefonoMovil: '',
    email: '',
    actividad: '',
    motivo: '',
    terms: false,
    modalVisible: false,
    dialogoRegistro: false,
    titleRegistro: false,

    institucion: '',
  };
  userRegistration = async () => {
    console.log(this.state);





    let urlCreation = 'http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/desaparecido/solicitud-usuario';

    const {
      cedula,
      nombre,
      direccion,
      telefonoOficina,
      telefonoDomicilio,
      telefonoMovil,
      email,
      actividad,
      motivo,
      terms,
      dialogVisible,
      institucion,

    } = this.state;

    if (nombre.length === 0) {
      alert('Falta el nombre');
      return;
    }
    if (cedula.length === 0) {
      alert('Falta la cédula');
      return;
    }
    if (direccion.length === 0) {
      alert('Falta la dirección');
      return;
    }
    if (telefonoOficina.length === 0) {
      alert('Falta el teléfono de oficina');
      return;
    }
    if (telefonoDomicilio.length === 0) {
      alert('Falta el teléfono de domicilio');
      return;
    }
    if (telefonoMovil.length === 0) {
      alert('Falta el teléfono móvil');
      return;
    }
    if (email.length === 0) {
      alert('Falta el email');
      return;
    }
    // if (motivo.length === 0) {
    //   alert('Falta el motivo');
    //   return;
    // }
    if (actividad.length === 0) {
      alert('Falta la actividad');
      return;
    }
    if (institucion.length === 0) {
      alert('Falta la institucion');
      return;
    }


    let bodyForm = {
      // actdedica:"nada",
      nombre: nombre,
      cedula: cedula,
      direccion: direccion,
      telfoficina: telefonoOficina,
      telfdomicilio: telefonoDomicilio,
      telfmovil: telefonoMovil,
      email: email,
      actdedica: actividad,
      motivo: motivo,

      // "direccion":"alpahuasi",
      // "nombre":"Alex",
      // "telfdomicilio":"sdsd",
      // "telfmovil":"dsdfsdf",
      // "telfoficina":"dsfdsf"

      institucion: institucion,

    }

    console.log(bodyForm);


    const options = {
      url: urlCreation,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: bodyForm
    };

    axios(options)
        .then(response => {

          console.log('ok');
          if (response.status=== 200){
            this.setState({titleRegistro: "REGISTRO CORRECTO"});
            this.setState({dialogoRegistro: true});
            this.setState({cedula: ''});
            this.setState({nombre: ''});
            this.setState({direccion: ''});
            this.setState({telefonoOficina: ''});
            this.setState({telefonoDomicilio: '' });
            this.setState({telefonoMovil: '' });
            this.setState({email: '' });
            this.setState({actividad: '' });
            this.setState({motivo: '' });
            this.setState({institucion: '' });
            this.setState({terms: false});
            // this.setState({dialogVisible: false});
          }
        }).catch(error => {
          console.log(error);
          this.setState({dialogVisible: false});
        this.setState({dialogoRegistro: true});

        this.setState({titleRegistro: "ERROR DE REGISTRO"});
          // alert('Error de registro, por favor comprueba tu conexión a internet');
        });
  }

  // constructor() {
  //     super();
  //
  //     this.state = {
  //         textInputValue: ''
  //     }
  //   console.log(this.state);
  //
  // }


  render() {
    return (
      <View style={styles.container}>
        <Dialog
            visible={this.state.dialogoRegistro}
            title={this.state.titleRegistro}
            onTouchOutside={() => this.setState({dialogoRegistro: false})} >
          {/*<View>*/}
          {/*    // your content here*/}
          {/*</View>*/}
        </Dialog>

        <MenuButton navigation={this.props.navigation} />
        <View style={styles.headerActions}>
          {/*<TouchableOpacity style={styles.actionButton} onPress={() => this.goToUserLogin()}>*/}
          {/*  <FontAwesome name="sign-in" size={20} color="#053862" />*/}
          {/*  <Text style={styles.actionText}>Creación</Text>*/}
          {/*</TouchableOpacity>*/}
        </View>
        <KeyboardAvoidingView
          style={styles.userRegistrationForm}
          behavior="height"
          keyboardVerticalOffset={60}
        >
        <ScrollView style={styles.scroll}>

          <Text style={styles.formHeaders}>Datos Personales</Text>
          <TextInput
            style={styles.formInputs}
            placeholder="Número de cédula"
            value={this.state.cedula}
            onChange={cedula => this.setState({ cedula: cedula.nativeEvent.text })}
            keyboardType="phone-pad"
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Nombre"
            value={this.state.nombre}
            onChange={nombre => this.setState({ nombre: nombre.nativeEvent.text })}
          />

          <Text style={styles.formHeaders}>Datos de Contacto</Text>
          <TextInput
            style={styles.formInputs}
            placeholder="Dirección"
            value={this.state.direccion}
            onChange={direccion => this.setState({ direccion: direccion.nativeEvent.text })}
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Institución a la que pertenece"
            value={this.state.institucion}
            onChange={institucion => this.setState({ institucion: institucion.nativeEvent.text })}
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Teléfono de Oficina"
            value={this.state.telefonoOficina}
            onChange={telefonoOficina => this.setState({ telefonoOficina: telefonoOficina.nativeEvent.text })}
            keyboardType="phone-pad"
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Teléfono de Domicilio"
            value={this.state.telefonoDomicilio}
            onChange={telefonoDomicilio => this.setState({ telefonoDomicilio: telefonoDomicilio.nativeEvent.text })}
            keyboardType="phone-pad"
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Teléfono Móvil"
            value={this.state.telefonoMovil}
            onChange={telefonoMovil => this.setState({ telefonoMovil: telefonoMovil.nativeEvent.text })}
            keyboardType="phone-pad"
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Correo Electrónico"
            value={this.state.email}
            onChange={email => this.setState({ email: email.nativeEvent.text })}
            keyboardType="email-address"
          />
          <TextInput
            style={styles.formInputs}
            placeholder="Actividades a las que se dedica"
            value={this.state.actividad}
            onChange={actividad => this.setState({ actividad : actividad.nativeEvent.text})}
          />

          {/* <TextInput
            style={styles.formInputs}
            placeholder="Motivo"
            value={this.state.motivo}
            onChange={motivo => this.setState({ motivo: motivo.nativeEvent.text })}
          /> */}
          <Text style={styles.formHeadersTerminos}
          onPress={()=>this.setState({modalVisible: true})}
          >TÉRMINOS Y CONDICIONES</Text>

          <CheckBox
              style={{ padding: 10}}
              onClick={()=>{
                this.setState({
                  isChecked:!this.state.isChecked
                })
              }}
              isChecked={this.state.isChecked}
              rightText={"Acepto los términos y condiciones de uso"}
          />

          {this.state.isChecked && <View style={styles.buttonContainer} >
            <FormButton
                buttonType="outline"
                onPress={this.userRegistration}
                title="ENVIAR SOLICITUD"
                buttonColor="#039BE5"
            />
          </View>}

          <Modal isVisible={this.state.modalVisible}>
            <View style={styles.modalInfoCloseButtonContainer}>
              <TouchableOpacity
                  onPress={() => this.setState({modalVisible: false})}
                  style={{ margin: 25 }}
              >
                <FontAwesome name="close" size={35} color="#fff" />
              </TouchableOpacity>
            </View>


          <ScrollView style={styles.scrollView}>
            <Text style={[styles.textareaContainer, {fontWeight: 'bold',     textAlign: 'center'}]}>TÉRMINOS Y CONDICIONES</Text>
            <Text style={styles.textareaContainer}>
              I. Es un sistema que funciona a través de una aplicación creada por el Ministerio de Gobierno para afianzar la seguridad ciudadana y convivencia local, con la colaboración y contribución de todas las instituciones públicas, privadas y particulares, quienes podrán introducir información de personas que se encuentren en situación de abandono, sin identidad, ni referente familiar.
            </Text>
            <Text style={styles.textareaContainer}>
            II. Queda prohibido tanto para el sector público como privado y a los particulares, el mal uso de la aplicación o el mal uso de la información generada a través de este medio, será de exclusiva responsabilidad del solicitante y/o requirente del mismo.
            </Text>
            <Text style={styles.textareaContainer}>
            III. La funcionalidad de esta aplicación se podrá realizar desde los dispositivos IOS, Android y desde cualquier computador. Cuenta con mecanismos de seguridad para controlar el acceso y utilización de la información.
            </Text>
            <Text style={styles.textareaContainer}>
            IV. Para utilizar este servicio usted debe aceptar los siguientes Términos y Condiciones de Uso:
            </Text>
            <Text style={styles.textareaContainer}>
            V. Entiendo que, por medio de la presente aplicación se podrá realizar desde los dispositivos IOS, Android y desde cualquier computador, el Ministerio de Gobierno pone a mi alcance los medios para que yo pueda compartir información de personas en situación de abandono, sin identidad, ni referente familiar, en aras de fomentar su reintegración familiar, de conformidad con los artículos 66 numeral 19, artículos 85, 154 numeral 1, artículos 226, 227, 322 de la Constitución de la República del Ecuador; de conformidad el artículos 14, 43 y 44 de la Ley Orgánica de Actuación en Casos de Personas Desaparecidas y Extraviadas; así como el artículo 102 del Código Orgánico de la Economía Social de los Conocimientos, Creatividad e Innovación y demás legislación aplicable. Le informamos que la plataforma Desaparecidos Ecuador es una creación perteneciente al Ministerio de Gobierno.
            </Text>
            <Text style={styles.textareaContainer}>
            VI. Entiendo que, el Ministerio de Gobierno del Ecuador es responsable de asegurar la concordancia entre los datos que le han sido suministrados y los que registra/divulga.
            </Text>
            <Text style={styles.textareaContainer}>
            VII. Acepto que en cualquier momento y sin previo aviso, el Ministerio de Gobierno del Ecuador puede modificar o actualizar este servicio informático en conveniencia de sus obligaciones.
            </Text>
            <Text style={styles.textareaContainer}>
            VIII. Declaro que me hago responsable por el uso y cuidado de la información que pueda llegar a obtener a través del presente servicio informático.
            </Text>
          </ScrollView>
          </Modal>

          {/*<View style={styles.containerText}>*/}
          {/*  <Text style={styles.textareaContainer}>*/}
          {/*    I. Es un sistema que funciona a través de una aplicación creada por el Ministerio de Gobierno para afianzar la seguridad ciudadana y convivencia local, con la colaboración y contribución de todas las instituciones públicas, privadas y particulares, quienes podrán introducir información de personas que se encuentren en situación de abandono, sin identidad, ni referente familiar.*/}

          {/*    II. Queda prohibido tanto para el sector público como privado y a los particulares, el mal uso de la aplicación o el mal uso de la información generada a través de este medio, será de exclusiva responsabilidad del solicitante y/o requirente del mismo.*/}

          {/*    III. La funcionalidad de esta aplicación se podrá realizar desde los dispositivos IOS, Android y desde cualquier computador. Cuenta con mecanismos de seguridad para controlar el acceso y utilización de la información.*/}

          {/*    IV. Para utilizar este servicio usted debe aceptar los siguientes Términos y Condiciones de Uso:*/}

          {/*    V. Entiendo que, por medio de la presente aplicación se podrá realizar desde los dispositivos IOS, Android y desde cualquier computador, el Ministerio de Gobierno pone a mi alcance los medios para que yo pueda compartir información de personas en situación de abandono, sin identidad, ni referente familiar, en aras de fomentar su reintegración familiar, de conformidad con los artículos 66 numeral 19, artículos 85, 154 numeral 1, artículos 226, 227, 322 de la Constitución de la República del Ecuador; de conformidad el artículos 14, 43 y 44 de la Ley Orgánica de Actuación en Casos de Personas Desaparecidas y Extraviadas; así como el artículo 102 del Código Orgánico de la Economía Social de los Conocimientos, Creatividad e Innovación y demás legislación aplicable. Le informamos que la plataforma Desaparecidos Ecuador es una creación perteneciente al Ministerio de Gobierno.*/}

          {/*    VI. Entiendo que, el Ministerio de Gobierno del Ecuador es responsable de asegurar la concordancia entre los datos que le han sido suministrados y los que registra/divulga.*/}

          {/*    VII. Acepto que en cualquier momento y sin previo aviso, el Ministerio de Gobierno del Ecuador puede modificar o actualizar este servicio informático en conveniencia de sus obligaciones.*/}

          {/*    VIII. Declaro que me hago responsable por el uso y cuidado de la información que pueda llegar a obtener a través del presente servicio informático.*/}
          {/*  </Text>*/}


          {/*  <Textarea*/}
          {/*      containerStyle={styles.textareaContainer}*/}
          {/*      style={styles.textarea}*/}
          {/*      onChangeText={this.onChange}*/}
          {/*      defaultValue={this.state.text}*/}
          {/*      maxLength={120}*/}
          {/*      placeholder={'好玩有趣的，大家同乐，伤感忧闷的，大家同哭。。。'}*/}
          {/*      placeholderTextColor={'#c7c7c7'}*/}
          {/*      underlineColorAndroid={'transparent'}*/}
          {/*  />*/}
          {/*</View>*/}
          </ScrollView>
        </KeyboardAvoidingView>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  headerActions: {
    flexDirection: 'row',
    height: 60,
    width: '100%',
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: Constants.statusBarHeight
  },
  actionButton: {
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  actionText: {
    marginLeft: 10,
    fontWeight: '600',
    color: '#053862',
    fontSize: 14
  },
  userRegistrationForm: {
    display: 'flex',
    flex: 1,
    width: '95%',
    flexDirection: 'column'
  },
  formHeaders: {
    width: '100%',
    height: 40,
    backgroundColor: '#ECECEC',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 16,
    color: '#053862',
    borderColor: '#A5A5A5',
    borderRadius: 5
  },
  formHeadersTerminos: {
    width: '100%',
    height: 40,
    backgroundColor: '#ECECEC',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 16,
    color: '#0000ff',
    borderColor: '#A5A5A5',
    borderRadius: 5
  },
  formInputs: {
    borderColor: '#ECECEC',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 5,
    height: 30,
    marginHorizontal: 5,
    marginVertical: 5
  },
  containerText: {
    flex: 1,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textareaContainer: {
    // height: 180,
    padding: 5,
    backgroundColor: '#F5FCFF',
    textAlign: 'justify',
    margin: 5,

  },
  textarea: {
    textAlignVertical: 'top',  // hack android
    height: 170,
    fontSize: 14,
    color: '#333',
  },
  scrollView: {
    backgroundColor: 'white',
    marginHorizontal: 20,
  },
  buttonContainer: {
    margin: 25
  },

  modalInfoCloseButtonContainer: {
    // flex: 1,
    // height: 20,
    alignItems: 'flex-end'
  },

});
