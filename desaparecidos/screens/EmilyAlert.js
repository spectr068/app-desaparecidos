import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import MenuButton from '../components/MenuButton';
// import Accordion from 'react-native-collapsible/Accordion';
import { FontAwesome } from '@expo/vector-icons';
import Constants from 'expo-constants';

import WhatIsEmilyAlert from '../components/WhatIsEmilyAlert';
import HowItWorksEmilyAlert from '../components/HowItWorksEmilyAlert';
import WhatToDoEmilyAlert from '../components/WhatToDoEmilyAlert';
import SolvedCases from '../components/SolvedCases';
import { ScrollView } from 'react-native-gesture-handler';

export default class EmilyAlert extends React.Component {
  state = {
    activeSections: []
  };

  _renderHeader = (section, index, isActive, sections) => {
    return (
      <View style={styles.accordionItem_header}>
        <Text style={styles.accordionItem_headerText}>{section.title}</Text>
        <FontAwesome name={isActive ? 'minus' : 'plus'} size={20} color="white" />
      </View>
    );
  };

  _renderContent = section => {
    if (section.key === 'whatisemilyalert') {
      return (
        <View style={styles.accordionItem_content}>
          <WhatIsEmilyAlert />
        </View>
      );
    }
    if (section.key === 'howitworksemilyalert') {
      return (
        <View style={styles.accordionItem_content}>
          <HowItWorksEmilyAlert />
        </View>
      );
    }
    if (section.key === 'whattodoemilyalert') {
      return (
        <View style={styles.accordionItem_content}>
          <WhatToDoEmilyAlert />
        </View>
      );
    }
    if (section.key === 'solvedcases') {
      return (
        <View style={styles.accordionItem_content}>
          <SolvedCases />
        </View>
      );
    }
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  render() {
    return (
      <View style={styles.container}>
        <MenuButton navigation={this.props.navigation} />
        <View style={styles.headerView}>
          <Image style={styles.headerView_logo} source={require('../assets/main-logo.png')} />
        </View>
        <ScrollView style={styles.contentContainer}>
          {/*<Accordion*/}
          {/*  sections={SECTIONS}*/}
          {/*  activeSections={this.state.activeSections}*/}
          {/*  underlayColor="#03C8D5"*/}
          {/*  renderHeader={this._renderHeader}*/}
          {/*  renderContent={this._renderContent}*/}
          {/*  onChange={this._updateSections}*/}
          {/*  containerStyle={styles.accordionContainer}*/}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerView: {
    width: '50%',
    height: 80,
    alignSelf: 'center',
    marginTop: Constants.statusBarHeight
  },
  headerView_logo: {
    width: '100%',
    height: '100%'
  },
  contentContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#FF9548'
  },
  accordionContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: '#0E406B'
  },
  accordionItem_header: {
    backgroundColor: '#FF9548',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    height: 70
  },
  accordionItem_headerText: {
    fontSize: 20,
    color: '#fff'
  },
  accordionItem_content: {
    paddingVertical: 10,
    backgroundColor: '#FEFFD4'
  }
});

const SECTIONS = [
  {
    title: '¿QUE ES ALERTA EMILIA?',
    key: 'whatisemilyalert'
  },
  {
    title: '¿COMÓ FUNCIONA?',
    key: 'howitworksemilyalert'
  },
  {
    title: '¿QUÉ HACER?',
    key: 'whattodoemilyalert'
  },
  {
    title: 'CASOS',
    key: 'solvedcases'
  }
];
