import React, { Component, Fragment } from 'react';
import {
    Dimensions,
    Image,
    ImageBackground,
    Linking,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import axios from 'axios';
import Constants from "expo-constants";

const pictureUrlApiBase = 'http://181.113.21.12/images/desaparecidos/';

export default class MissingPeopleCard extends Component {
  state = {
    modalVisible: false,
    person: {}
  };

  setModalVisible = async (visible, idDesaparecido) => {
    try {
      const response = await axios.get(
        `http://admincore.ministeriodegobierno.gob.ec:28080/registroservicios-war/webresources/api/busqueda/desaparecidos/id/${idDesaparecido}`
      );
      this.setState({ modalVisible: visible, person: response.data });
    } catch (error) {
      console.error(error);
    }
  };

  onShareFacebook = idDesaparecido => {
    Linking.openURL(
      `https://www.facebook.com/sharer.php?u=http://desaparecidosecuador.gob.ec/persona/${idDesaparecido}`
    );
  };

  onShareTwitter = idDesaparecido => {
    Linking.openURL(
      `https://twitter.com/intent/tweet?text=Ayúdanos a encontrar a ABAD CHUQUIHUANGA FELISARDO más detalles en http://www.desaparecidosecuador.gob.ec/persona/${idDesaparecido}`
    );
  };

  getAfiche = idDesaparecido => {
    Linking.openURL(`http://desaparecidosecuador.gob.ec/reporte-${idDesaparecido}`);
  };

  render() {
    const { idDesaparecido, nombres, pictureId, fechaDesaparicion } = this.props;
    return (
      <Fragment>
        <View key={idDesaparecido} style={styles.missingPeopleContainer}>
          <TouchableOpacity
            style={styles.imageContainer}
            onPress={() => this.setModalVisible(true, idDesaparecido)}
          >
            <Image
              style={styles.personImage}
              source={{ uri: `${pictureUrlApiBase}${pictureId}` }}
            />

          </TouchableOpacity>
            <View style={styles.infoContainer}>
                <Text style={styles.titleInfo}>{nombres}</Text>
                <Text style={styles.aditionalInfo}>Fue visto por última vez el {fechaDesaparicion}</Text>
                {/*<View style={styles.socialLinksContainer}>*/}

                {/*</View>*/}
            </View>

            {/*<TouchableOpacity onPress={() => this.onShareFacebook(idDesaparecido)}>*/}
              {/*<FontAwesome name="facebook-square" size={30} color="#053862" />*/}
            {/*</TouchableOpacity>*/}
            {/*<TouchableOpacity onPress={() => this.onShareTwitter(idDesaparecido)}>*/}
              {/*<FontAwesome name="twitter-square" size={30} color="#053862" />*/}
            {/*</TouchableOpacity>*/}
            {/*<TouchableOpacity onPress={() => this.setModalVisible(true, idDesaparecido)}>*/}
              {/*<FontAwesome name="info-circle" size={30} color="#053862" />*/}
            {/*</TouchableOpacity>*/}


          <Modal
              style={styles.modalContent}
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({ modalVisible: !this.state.modalVisible });
            }}
          >
            <View style={styles.modalInfo}>

                <View style={styles.modalInfoCloseButtonContainer}>
                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}
                        style={{}}
                    >
                        <FontAwesome name="close" size={40} color="#053862" />
                    </TouchableOpacity>
                </View>
            </View>
              <View style={styles.modalInfoContainer}>
                  <Text style={styles.namePeopleCard}>{this.state.person.nombre}</Text>
                  <Image
                      style={styles.personImageCard}
                      source={{ uri: `${pictureUrlApiBase}${pictureId}` }}
                  />
                  <View styles={styles.personInfoContainer}>
                      <Text style={styles.textInfoPerson}>Edad actual: {this.state.person.edad} años</Text>
                      <Text style={styles.textInfoPerson}>Fecha de nacimiento: {this.state.person.fechaNacimiento}</Text>
                      <Text style={styles.textInfoPerson}>Contextura: {this.state.person.contextura}</Text>
                      <Text style={styles.textInfoPerson}>De género: {this.state.person.genero}</Text>
                      <Text style={styles.textInfoPerson}>De nacionalidad: {this.state.person.nacionalidad}</Text>
                      <Text style={styles.textInfoPerson}>Tiene ojos color: {this.state.person.colorOjos}</Text>
                      <Text style={styles.textInfoPerson}>Tiene cabello color: {this.state.person.colorCabello}</Text>
                      <Text style={styles.textInfoPerson}>Estatura: {this.state.person.statura} cm</Text>
                      <Text style={styles.textInfoPerson}>Peso: {this.state.person.peso} libras</Text>
                      <Text style={styles.textInfoPerson}>
                          Características particulares: {this.state.person.caracteristicasParticulares}
                      </Text>
                  </View>

                <View>

                      <Text></Text>
                      <Text style={styles.textInfoPersonFin}>
                          Si tienes información de esta persona llámanos o escríbenos en cualquier momento de manera
                          gratuita y confidencial: 1800 DELITO (33 54 86) / desaparecidosecuador@ministeriodegobierno.gob.ec
                      </Text>
                  </View>
                  <View style={styles.personActionsContainer}>
                      {/*<TouchableOpacity style={styles.personActionButton}>*/}
                      {/*    <Text style={styles.textButton}>Sé algo de la persona</Text>*/}
                      {/*</TouchableOpacity>*/}
                      <TouchableOpacity
                          style={styles.personActionButton}
                          onPress={() => this.getAfiche(idDesaparecido)}
                      >
                          <Text style={styles.textButton}>Descargar afiche</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={styles.personActionsContainer}>

                    <TouchableOpacity style={styles.buttonSocial} onPress={() => this.onShareFacebook(idDesaparecido)}>
                      <FontAwesome name="facebook-square" size={30} color="#053862" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.buttonSocial} onPress={() => this.onShareTwitter(idDesaparecido)}>
                      <FontAwesome name="twitter-square" size={30} color="#053862" />
                    </TouchableOpacity>
                    {/*<TouchableOpacity style={styles.buttonSocial} onPress={() => this.setModalVisible(true, idDesaparecido)}>*/}
                      {/*<FontAwesome name="info-circle" size={30} color="#053862" />*/}
                    {/*</TouchableOpacity>*/}
                  </View>
              </View>

            {/*<ImageBackground*/}
              {/*source={{ uri: `${pictureUrlApiBase}${pictureId}` }}*/}
              {/*style={styles.imageBackground}*/}
              {/*resizeMode="stretch"*/}
            {/*>*/}
              {/*<View style={styles.modalInfo}>*/}
                {/*/!*<View style={styles.modalInfoCloseButtonContainer}>*!/*/}
                  {/*/!*<TouchableOpacity*!/*/}
                    {/*/!*onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}*!/*/}
                    {/*/!*style={{ margin: 10 }}*!/*/}
                  {/*/!*>*!/*/}
                    {/*/!*<FontAwesome name="close" size={25} color="#053862" />*!/*/}
                  {/*/!*</TouchableOpacity>*!/*/}
                {/*/!*</View>*!/*/}
                {/*<View style={styles.infoContainer}>*/}
                  {/*<Text style={{ alignSelf: 'center' }}>{this.state.person.nombre}</Text>*/}

                  {/*<View styles={styles.personInfoContainer}>*/}
                    {/*<Text>Edad actual: {this.state.person.edad} años</Text>*/}
                    {/*<Text>Fecha de nacimiento: {this.state.person.fechaNacimiento}</Text>*/}
                    {/*<Text>Contextura: {this.state.person.contextura}</Text>*/}
                    {/*<Text>De género: {this.state.person.genero}</Text>*/}
                    {/*<Text>De nacionalidad: {this.state.person.nacionalidad}</Text>*/}
                    {/*<Text>Tiene ojos color: {this.state.person.colorOjos}</Text>*/}
                    {/*<Text>Tiene cabello color: {this.state.person.colorCabello}</Text>*/}
                    {/*<Text>Estatura: {this.state.person.statura} cm</Text>*/}
                    {/*<Text>Peso: {this.state.person.peso} libras</Text>*/}
                    {/*<Text>*/}
                      {/*Características particulares: {this.state.person.caracteristicasParticulares}*/}
                    {/*</Text>*/}

                    {/*<Text></Text>*/}
                    {/*<Text style={{ textAlign: 'center' }}>*/}
                        {/*Si tienes información de esta persona llámanos o escríbenos en cualquier momento*/}
                        {/*Gratis y confidencial: 1800 DELITO (33 54 86) / desaparecidosecuador@mdi.gob.ec*/}
                    {/*</Text>*/}
                  {/*</View>*/}
                    {/*/!*<View style={styles.personActionsContainer}>*!/*/}
                        {/*/!*<TouchableOpacity style={styles.personActionButton}>*!/*/}
                            {/*/!*<Text>Se algo de la persona</Text>*!/*/}
                        {/*/!*</TouchableOpacity>*!/*/}
                        {/*/!*<TouchableOpacity*!/*/}
                            {/*/!*style={styles.personActionButton}*!/*/}
                            {/*/!*onPress={() => this.getAfiche(idDesaparecido)}*!/*/}
                        {/*/!*>*!/*/}
                            {/*/!*<Text>Descargar afiche</Text>*!/*/}
                        {/*/!*</TouchableOpacity>*!/*/}
                    {/*/!*</View>*!/*/}

                {/*</View>*/}

              {/*</View>*/}
            {/*</ImageBackground>*/}
          </Modal>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  missingPeopleContainer: {
    flex: 1,
    width: (Dimensions.get('window').width - 20),
    height: (Dimensions.get('window').height - 80 -50 -20) /6,
    alignItems: 'center',
    flexDirection: 'row',
    margin: 10,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    // justifyContent: 'center'
  },
  imageContainer: {
    flex:1,
    width: '90%',
    height: '100%'
  },
  personImage: {
    flex: 1,
      height: '95%',
      width: undefined,
      aspectRatio: 1,

    resizeMode: 'stretch',
    borderRadius: 3
  },
  infoContainer: {
    flex: 2,
    width: '100%',
      alignItems: 'center',

      // backgroundColor: '#00c000'
    // height: 250,
    // padding: 10
  },
  modalInfoContainer: {
    // flex: 2,
    width: '90%',
      height: '85%',
      alignItems: 'center',
      alignSelf: 'center',

      // backgroundColor: '#00ACBF',
      backgroundColor: '#00dfF8',
      borderColor: '#fff',
      borderWidth: 2,
    // height: 250,
    // padding: 10
  },
  namePeopleCard: {
      color: '#fff',
      fontSize: (Dimensions.get('window').width) / 22,
      backgroundColor: '#074d57',
      alignItems: 'center',

  },

    headerView: {
        backgroundColor: '#074d57',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 80,
        alignSelf: 'center',
        marginTop: Constants.statusBarHeight /2,
        marginBottom: Constants.statusBarHeight /2,
    },

  socialLinksContainer: {
    width: '100%',
    height: undefined,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  imageBackground: {
    flex: 1,
    margin: 20,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#000'
  },
  modalInfo: {
    backgroundColor: '#fff',

    // backgroundColor: 'rgba(255,255,255,1)',
    // flex: 1
  },

    modalContent:{
    // width: (Dimensions.get('window').width - 20),
        height: '90%',
        alignItems: 'center',
        alignSelf: 'center',


    },

  modalInfoCloseButtonContainer: {
    // flex: 1,
    // height: 20,
    alignItems: 'flex-end'
  },

    personImageCard: {
        // flex: 1,
        width: (Dimensions.get('window').width * 0.50),
        height: undefined,

        aspectRatio: 1,

        resizeMode: 'stretch',
        borderRadius: 3
    },

  personInfoContainer: {
    width: (Dimensions.get('window').width * 0.5),

      backgroundColor: '#fff',
      // alignItems: 'center',

      // padding: 20,
    // marginBottom: 40
  },
  textInfoPerson: {
      fontSize: (Dimensions.get('window').width) / 32,
  }  ,

    textInfoPersonFin: {
      fontSize: (Dimensions.get('window').width) / 35,
        textAlign: 'center',

    }  ,

  personExtraInfoContainer: {
    // paddingTop: 40,
    flex: 2,
    alignItems: 'center'
  },
  personActionsContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  personActionButton: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    backgroundColor: '#074d57'
  },

    textButton: {
        color: '#fff',
    },
    titleInfo: {
        color: '#fff',
        fontSize: (Dimensions.get('window').width) / 25 ,
        alignItems: 'center',
        justifyContent: 'center',
    },
    aditionalInfo: {
        color: '#000',
        fontSize: (Dimensions.get('window').width) / 28,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonSocial:{
      flex: 1,
        alignItems: 'center',
        // width: '50%',

    }
});
